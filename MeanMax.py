import sys
import math


# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

def dist(object1, object2):
    return math.sqrt((object1.x - object2.x) ** 2 + (object1.y - object2.y) ** 2)

class Command(object):
    def __init__(self,x,y,p):
        self.x = x
        self.y = y
        self.p = p

    def __repr__(self):
        return "%d %d %d" % (int(self.x), int(self.y), self.p)

class Unit(object):
    def __init__(self, unit_id, player_id, mass, radius, x, y, vx, vy, extra, extra_2):
        self.unit_id = unit_id
        self.player_id = player_id
        self.mass = mass
        self.radius = radius
        self.x = x
        self.y = y
        self.vx = vx
        self.vy = vy
        self.extra = extra
        self.extra_2 = extra_2

def closest_epave(entity):
    closest = None
    for epave in epaves:
        if not closest or dist(entity, epave) < dist(entity, closest):
            closest = epave
    return closest

def closest_sac(entity):
    closest = None
    for sac in gros_sacs:
        if not closest or dist(entity, sac) < dist(entity, closest):
            closest = sac
    return closest

# game loop
while True:
    my_score = int(input())
    enemy_score_1 = int(input())
    enemy_score_2 = int(input())
    my_rage = int(input())
    enemy_rage_1 = int(input())
    enemy_rage_2 = int(input())
    unit_count = int(input())
    epaves = []
    gros_sacs = []
    for i in range(unit_count):
        unit_id, unit_type, player_id, mass, radius, x, y, vx, vy, extra, extra_2 = input().split()
        unit_id = int(unit_id)
        unit_type = int(unit_type)
        player_id = int(player_id)
        x = int(x)
        y = int(y)
        vx = int(vx)
        vy = int(vy)
        if unit_type == 4:
            epaves.append(Unit(unit_id, player_id, mass, radius, x, y, vx, vy, extra, extra_2))
        elif unit_type == 0:
            if player_id == 0:
                reaper = Unit(unit_id, player_id, mass, radius, x, y, vx, vy, extra, extra_2)
            elif player_id == 1:
                reaper_1 = Unit(unit_id, player_id, mass, radius, x, y, vx, vy, extra, extra_2)
            elif player_id == 2:
                reaper_2 = Unit(unit_id, player_id, mass, radius, x, y, vx, vy, extra, extra_2)
        elif unit_type == 3:
            gros_sacs.append(Unit(unit_id, player_id, mass, radius, x, y, vx, vy, extra, extra_2))

    closest_reaper = closest_epave(reaper)
    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)
    if closest_reaper:
        print(closest_reaper.x, closest_reaper.y, 300)
    else:
        x = 0
        y = 0
        for gros_sac in gros_sacs:
            x += gros_sac.x
            y += gros_sac.y
        x = int(x / len(gros_sacs))
        y = int(y / len(gros_sacs))
        print(x, y, 300)
    doofer_target = Command((reaper_1.x + closest_sac(reaper_1).x) / 2, (reaper_1.y + closest_sac(reaper_1).y) / 2)
    print(doofer_target)
    destroyer_target = Command((reaper_2.x + closest_sac(reaper_2).x) / 2, (reaper_2.y + closest_sac(reaper_2).y) / 2)
    print(destroyer_target)
