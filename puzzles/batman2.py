import sys
import math

w = 10000
h = 10000
# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

class Game(object):
    def __init__(self, w, h):
        self.width = w
        self.heigth = h
        self.bomb_zone = Zone(0, w-1, 0, h-1)

    def reduce_zone(self, pos1, pos2, bomb_dir):
        if bomb_dir == "COLDER":
            self.colder(pos1, pos2)
        elif bomb_dir == "WARMER":
            self.warmer(pos1, pos2)
        elif bomb_dir == "SAME":
            self.same(pos1, pos2)
        else:
            pass

    def colder(self, pos1, pos2):
        self.bomb_zone.closer(pos1,pos2)

    def warmer(self, pos1, pos2):
        self.bomb_zone.closer(pos2, pos1)

    def same(self, pos1, pos2):
        self.bomb_zone.same(pos1,pos2)

    def next_move(self, position):
        print(self.bomb_zone, file=sys.stderr)
        if self.bomb_zone.size()<=1:
            return self.get_valid_pos()
        elif self.bomb_zone.x_range() > self.bomb_zone.y_range():
            #mirror x through median axe of zone in x
            print("posx", position.x, "mid", self.bomb_zone.mid_x(), file=sys.stderr)
            return Position(int(2 * self.bomb_zone.mid_x() - position.x), position.y)
        else:
            return Position(position.x, int(2 * self.bomb_zone.mid_y() - position.y))

    def get_valid_pos(self):
        return Position(self.bomb_zone.min_x, self.bomb_zone.min_y)


class Zone(object):
    def __init__(self, min_x, max_x, min_y, max_y):
        self.min_x = min_x
        self.max_x = max_x
        self.min_y = min_y
        self.max_y = max_y

    def size(self):
        return self.x_range() * self.y_range()

    def closer(self, pos1, pos2):
        if pos1.x == pos2.x:
            if pos1.y>pos2.y:
                self.min_y = int(math.ceil((pos1.y+pos2.y)/2+0.1))
                print("min:", self.mid_y(), file=sys.stderr)
            else:
                self.max_y = int((pos1.y+pos2.y)/2-0.1)
                print("maxy:", pos1.y, pos2.y, file=sys.stderr)
        else:
            if pos1.x > pos2.x:
                self.min_x = int(math.ceil((pos1.x+pos2.x)/2+0.1))
                print(self.mid_x(), file=sys.stderr)
            else:
                self.max_x = int((pos1.x+pos2.x)/2-0.1)
                print("maxx:", self.mid_x(), file=sys.stderr)

    def same(self, pos1, pos2):
        if pos1.x == pos2.x:
            self.min_y = int(self.mid_y())
            self.max_y = int(self.mid_y())
        else:
            self.min_x = int(self.mid_x())
            self.max_x = int(self.mid_x())

    def y_range(self):
        return self.max_y - self.min_y + 1

    def x_range(self):
        return self.max_x - self.min_x + 1

    def mid_x(self):
        return (self.max_x + self.min_x) / 2

    def mid_y(self):
        return (self.max_y + self.min_y) / 2

    def __repr__(self):
        return "x:[%d,%d] y:[%d,%d]" % (self.min_x, self.max_x, self.min_y, self.max_y)


class Position(object):
    def __init__(self, x, y):
        self.x = 0 if x<0 else w-1 if x>w-1 else x
        self.y = 0 if y<0 else h-1 if y>h-1 else y

    def __repr__(self):
        return str(self.x) + " " + str(self.y)

    def __eq__(self, other):
        if self.x == other.x and self.y == other.y:
            return True
        return False

if __name__ == '__main__':
    # w: width of the building.
    # h: height of the building.
    w, h = [int(i) for i in input().split()]
    n = int(input())  # maximum number of turns before game over.
    x0, y0 = [int(i) for i in input().split()]
    previous_positions = [Position(x0, y0), Position(x0, y0)]
    game = Game(w,h)
    # game loop
    while True:
        bomb_dir = input()  # Current distance to the bomb compared to previous distance (COLDER, WARMER, SAME or UNKNOWN)
        game.reduce_zone(previous_positions[-2], previous_positions[-1], bomb_dir)
        print(previous_positions[-1], file=sys.stderr)
        new_pos = game.next_move(previous_positions[-1])
        if new_pos == previous_positions[-1]:
            new_pos = game.get_valid_pos()
        print(new_pos)
        previous_positions.append(new_pos)

