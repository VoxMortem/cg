import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

# w: width of the building.
# h: height of the building.
w, h = [int(i) for i in input().split()]
n = int(input())  # maximum number of turns before game over.
x0, y0 = [int(i) for i in input().split()]
x = x0
y = y0
cells_left = {'width': [0, w], 'heigth': [0, h]}
# game loop
while True:
    bomb_dir = input()  # Current distance to the bomb compared to previous distance (COLDER, WARMER, SAME or UNKNOWN)
    print(cells_left, file=sys.stderr)
    if bomb_dir != 'UNKNOWN':
        print(bomb_dir, file=sys.stderr)
    x0 = x
    y0 = y
    if cells_left['width'][1] - cells_left['width'][0] > cells_left['heigth'][1] - cells_left['heigth'][0]:
        # horizontal
        direction = 'width'
        width = cells_left['width'][1] - cells_left['width'][0]
        middle = width / 2 + cells_left['width'][0]
        x = 2 * middle - x0
        print(x, file=sys.stderr)
        x = int(x)
    else:
        # vertical
        direction = 'heigth'
        heigth = cells_left['heigth'][1] - cells_left['heigth'][0]
        middle = heigth / 2 + cells_left['heigth'][0]
        y = 2 * middle - y0
        print(y, file=sys.stderr)
        y = int(y)
    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)

    print(x, y)