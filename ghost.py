import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.
import time

start_time=time.time()

class WatchdogTimeoutError(RuntimeError):
    """Raised in case of runtime limit violations."""

def init():
    factory_count = int(input())  # the number of factories
    link_count = int(input())  # the number of links between factories
    cartographie = {}
    for i in range(link_count):
        factory_1, factory_2, distance = [int(j) for j in input().split()]
        if not factory_1 in cartographie:
            cartographie[factory_1] = {}
        if not factory_2 in cartographie:
            cartographie[factory_2] = {}
        cartographie[factory_1][factory_2] = distance
        cartographie[factory_2][factory_1] = distance
    return cartographie

class Factory(object):
    def __init__(self, id, proprio, soldats, prod):
        self.proprio = proprio
        self.soldats = soldats
        self.prod = prod
        self.id = id

    def __repr__(self):
        return str(self.__dict__)

class Troop(object):
    def __init__(self, id, proprio, origin, destination, nb, turns):
        self.proprio = proprio
        self.origin = origin
        self.destination = destination
        self.nb = nb
        self.turns = turns
        self.id = id

    def __repr__(self):
        return str(self.__dict__)

def tracer(frame, event, arg):
    "Helper."
    now = time.time()
    if now > start_time + 0.049:
        print("spent", now - start_time, file=sys.stderr)
        raise WatchdogTimeoutError(start_time, now)
    return tracer if event == "call" else None


class Game(object):
    def __init__(self, carto):
        self.carto = carto
        self.factorylist = []
        self.trooplist = []
        self.bomblist = []

    def reset(self):
        self.factorylist = []
        self.our_facts = []
        self.other_facts = []
        self.trooplist = []
        self.enemy_troops = []
        self.our_troops = []
        self.bomblist = []

    def add_factory(self, fact):
        self.factorylist.append(fact)
        if fact.proprio == 1:
            self.our_facts.append(fact)
        else:
            self.other_facts.append(fact)

    def add_troop(self, fact):
        self.trooplist.append(fact)

    def add_bomb(self, fact):
        self.bomblist.append(fact)

    def move_trop_top(self):
        output = []
        our_facts = sorted(self.our_facts, key=lambda k: k.soldats)
        for myfact in our_facts:
            troops_left = myfact.soldats
            for troop in self.trooplist:
                if troop.destination == myfact.id:
                    if troop.proprio == -1:
                        troops_left -= troop.nb
            other_facts = sorted(self.other_facts, key=lambda k: (
                k.proprio == 0 and k.prod > 0, k.prod, 1000 - cartographie[myfact.id][k.id]), reverse=True)

            for enemyfact in other_facts:
                print("enemy fact:", enemyfact, file=sys.stderr)
                if troops_left <= 0:
                    break
                troops_produced = enemyfact.soldats + (enemyfact.prod * cartographie[myfact.id][enemyfact.id] * -1) * enemyfact.proprio
                print("troops_produced:", troops_produced, file=sys.stderr)
                virtual_troops = troops_produced
                for troop in self.trooplist:
                    if troop.destination == enemyfact.id:
                        if troop.proprio == -1:
                            virtual_troops += troop.nb
                        else:
                            virtual_troops -= troop.nb
                print("virtual_troops:", virtual_troops, file=sys.stderr)
                if virtual_troops > troops_left:
                    continue
                if virtual_troops <= 0:
                    continue
                if troops_left != myfact.soldats and enemyfact.prod == 0:
                    break
                troops_to_send = virtual_troops + 1
                if troops_to_send > myfact.soldats:
                    continue
                troops_left -= troops_to_send
                output.extend(["MOVE", str(myfact.id), str(enemyfact.id), str(troops_to_send), ';'])
        return output


class Bomb(object):
    def __init__(self, id, proprio, origin, destination, nb, turns):
        self.proprio = proprio
        self.origin = origin
        self.destination = destination
        self.nb = nb
        self.turns = turns
        self.id = id


    def __repr__(self):
        return str(self.__dict__)

def main_loop(self):
    game = Game(cartographie)
    # game loop
    while True:

        game.reset()
        entity_count = int(input())  # the number of entities (e.g. factories and troops)
        for i in range(entity_count):
            entity_id, entity_type, arg_1, arg_2, arg_3, arg_4, arg_5 = input().split()
            entity_id = int(entity_id)
            arg_1 = int(arg_1)
            arg_2 = int(arg_2)
            arg_3 = int(arg_3)
            arg_4 = int(arg_4)
            arg_5 = int(arg_5)
            if entity_type == "FACTORY":
                game.add_factory(Factory(entity_id, arg_1, arg_2, arg_3))
            elif entity_type == "TROOP":
                game.add_troop(Troop(entity_id, arg_1, arg_2, arg_3, arg_4, arg_5))
            elif entity_type == "BOMB":
                game.add_bomb(Bomb(entity_id, arg_1, arg_2, arg_3, arg_4, arg_5))

        # Write an action using print
        # To debug: print("Debug messages...", file=sys.stderr)
        old_tracer = sys.gettrace()
        move = ["WAIT"]
        try:
            global start_time
            start_time = time.time()
            sys.settrace(tracer)
            move = game.move_trop_top()
            move.pop(-1)

            # Any valid action, such as "WAIT" or "MOVE source destination cyborgs"
        except Exception as e:
            print("Exception:", e, type(e), file=sys.stderr)
        finally:
            if move:
                print(" ".join(move))
            else:
                print("WAIT")
            sys.settrace(old_tracer)

if __name__ == '__main__':
    cartographie = init()
    main_loop(cartographie)
