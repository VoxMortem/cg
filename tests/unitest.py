import four
import unittest

class MainTestUsingFile(unittest.TestCase):

    def test_danger_push(self):
        four.base_x = 0
        four.base_y = 0
        four.enemy_base_pos = [17630, 9000]


        aMonster = four.Monster(1, 300, 300, 0, 0, 10, 500, 500, 1, 1)
        self.assertEqual(aMonster.get_danger(), 5)

    def test_danger_ignore(self):
        four.base_x = 0
        four.base_y = 0
        four.enemy_base_pos = [17630, 9000]


        aMonster = four.Monster(1, 300, 300, 5, 0, 10, 500, 500, 1, 1)
        self.assertEqual(aMonster.get_danger(), 5)

    def test_danger_ignore2(self):
        four.base_x = 0
        four.base_y = 0
        four.enemy_base_pos = [17630, 9000]

        #Monster 99: Position: 14639 7647, Shield: 0, IsControlled: 0, Health: 21, Direction: 364 164, NearBase: 1, ThreatFor: 2, Danger: -1.3125

        aMonster = four.Monster(99, 14639, 7647, 0, 0, 21, 364, 164, 1, 2)
        self.assertEqual(aMonster.get_danger(), -1.3125)

    def test_dist(self):
        four.base_x = 0
        four.base_y = 0
        four.enemy_base_pos = [17630, 9000]


        aMonster = four.Monster(1, 300, 300, 5, 0, 10, 500, 500, 1, 1)
        self.assertEqual(aMonster.get_danger(), 5)
