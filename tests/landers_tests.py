import timeit
import unittest
from mars_landers_opti import *
from puzzles import batman2


class MainTestUsingFile(unittest.TestCase):
    def test_no_move(self):
        shuttle = Shuttle(2500, 2700, 0, 0, 550, 0, 0)
        angle_change = 0
        power_change = 0
        shuttle.next(angle_change, power_change)
        self.compareShuttles(shuttle, Shuttle(2500, 2698, 0, -4, 550, 0, 0))
        shuttle.next(angle_change, power_change)
        self.compareShuttles(shuttle, Shuttle(2500, 2693, 0, -7, 550, 0, 0))
        shuttle.next(angle_change, power_change)
        self.compareShuttles(shuttle, Shuttle(2500, 2683, 0, -11, 550, 0, 0))

    def test_vertical_thrust(self):
        shuttle = Shuttle(2500, 2700, 0, 0, 550, 0, 0)
        shuttle.next(0, 1)
        self.compareShuttles(shuttle, Shuttle(2500, 2699, 0, -3, 549, 0, 1))
        shuttle.next(0, 1)
        self.compareShuttles(shuttle, Shuttle(2500, 2695, 0, -4, 547, 0, 2))
        shuttle.next(0, 1)
        self.compareShuttles(shuttle, Shuttle(2500, 2690, 0, -5, 544, 0, 3))
        shuttle.next(0, 1)
        self.compareShuttles(shuttle, Shuttle(2500, 2685, 0, -5, 540, 0, 4))
        shuttle.next(0, 0)
        self.compareShuttles(shuttle, Shuttle(2500, 2681, 0, -5, 536, 0, 4))

    def test_rotation_no_thrust(self):
        shuttle = Shuttle(2500, 2700, 0, 0, 550, 0, 0)
        shuttle.next(15, 0)
        self.compareShuttles(shuttle, Shuttle(2500, 2698, 0, -4, 550, 15, 0))
        shuttle.next(15, 0)
        self.compareShuttles(shuttle, Shuttle(2500, 2693, 0, -7, 550, 30, 0))
        shuttle.next(15, 0)
        self.compareShuttles(shuttle, Shuttle(2500, 2683, 0, -11, 550, 45, 0))
        shuttle.next(15, 0)
        self.compareShuttles(shuttle, Shuttle(2500, 2670, 0, -15, 550, 60, 0))
        shuttle.next(15, 0)
        self.compareShuttles(shuttle, Shuttle(2500, 2654, 0, -19, 550, 75, 0))

    def test_inclined_thrust(self):
        shuttle = Shuttle(2500, 2700, 0, 0, 550, 0, 0)
        shuttle.next(15, 1)
        self.compareShuttles(shuttle, Shuttle(2500, 2699, 0, -3, 549, 15, 1))
        shuttle.next(0, 1)
        self.compareShuttles(shuttle, Shuttle(2499, 2695, -1, -5, 547, 15, 2))
        shuttle.next(0, 1)
        self.compareShuttles(shuttle, Shuttle(2498, 2690, -2, -5, 544, 15, 3))
        shuttle.next(0, 1)
        self.compareShuttles(shuttle, Shuttle(2496, 2685, -3, -5, 540, 15, 4))
        shuttle.next(0, 0)
        self.compareShuttles(shuttle, Shuttle(2493, 2680, -4, -5, 536, 15, 4))

    def compareShuttles(self, a, b):
        try:
            self.assertEqual(a, b)
        except AssertionError as e:
            print(a)
            print(b)
            raise e

    def test_generate_random_commands(self):
        shuttle = Shuttle(2500, 2700, 0, 0, 550, 0, 0)
        shuttle.generate_random_commands()
        self.assertEqual(len(shuttle.commands), CHROM_SIZE)

    def test_square_collision(self):
        topo = Topo()
        topo.add([0, 0])
        topo.add([100, 0])
        topo.finalize()
        self.assertEqual(topo.check_collision(Line(Point(20, 20), Point(70, 70))), None)
        self.assertEqual(topo.check_collision(Line(Point(20, 20), Point(20, -20))), Point(20, 0))

    def test_crossing_point(self):
        line1 = Line(Point(10, 0), Point(0, 10))
        line2 = Line(Point(0, 0), Point(10, 10))
        self.assertEqual(line1.crossing_point(line2), Point(5, 5))
        self.assertEqual(line2.crossing_point(line1), Point(5, 5))
        line3 = Line(Point(0, 100), Point(100, 100))
        line4 = Line(Point(20, 80), Point(20, 120))
        self.assertEqual(line3.crossing_point(line4), Point(20, 100))

    def test_oblique_collision(self):
        topo = Topo()
        topo.add([-10, 0])
        topo.add([0, 0])
        topo.add([100, 100])
        topo.finalize()
        self.assertEqual(topo.check_collision(Line(Point(20, 30), Point(70, 80))), None)
        self.assertEqual(topo.lines[1].crossing_point(Line(Point(20, 10), Point(20, 40))), Point(20, 20))
        self.assertEqual(topo.check_collision(Line(Point(20, 10), Point(20, 40))), Point(20, 20))
        self.assertEqual(topo.check_collision_number(Line(Point(20, 10), Point(20, 40))), 1)

    def test_calculate_score_zero(self):
        topo = Topo()
        topo.add([100, 100])
        topo.add([200, 100])
        topo.finalize()
        shuttle = Shuttle(150, 150, 0, 0, 550, 0, 0)
        while len(shuttle.commands) < CHROM_SIZE:
            shuttle.commands.append(Command(0, 0))
        self.assertEqual(len(shuttle.commands), CHROM_SIZE)
        shuttle.calculate_score_crash(topo)
        self.assertEqual(shuttle.score, -762.34)

    def test_calculate_score_non_zero(self):
        topo = Topo()
        topo.add([0, 0])
        topo.add([100, 0])
        topo.add([100, 100])
        topo.add([200, 200])
        topo.finalize()
        shuttle = Shuttle(150, 190, 0, 0, 550, 0, 0)
        while len(shuttle.commands) < CHROM_SIZE:
            shuttle.commands.append(Command(0, 0))
        self.assertEqual(len(shuttle.commands), CHROM_SIZE)
        shuttle.calculate_score_crash(topo)
        self.assertEqual(shuttle.score, 170.71067811865476)

    def test_calculate_score_simple(self):
        topo = Topo()
        topo.add([100, 100])
        topo.add([200, 100])
        topo.finalize()
        shuttle = Shuttle(150, 150, 0, 0, 550, 0, 0)
        self.assertEqual(shuttle.get_score(topo), 50)

    def test_calculate_score_not_aligned(self):
        topo = Topo()
        topo.add([100, 100])
        topo.add([200, 100])
        topo.add([500, 0])
        topo.finalize()
        shuttle = Shuttle(500, 500, 0, 0, 550, 0, 0)
        self.assertEqual(shuttle.get_score(topo), 500)

    def test_calculate_score_blocked(self):
        topo = Topo()
        topo.add([100, 100])
        topo.add([200, 100])
        topo.add([200, 200])
        topo.add([300, 100])
        topo.add([400, 0])
        topo.finalize()
        shuttle = Shuttle(400, 200, 0, 0, 550, 0, 0)
        self.assertEqual(shuttle.get_score(topo), 300)


    def test_equality(self):
        self.compareShuttles(Shuttle(2500.1, 2697.8, 0.1, -3.51, 550, 0, 0), Shuttle(2500, 2698, 0, -4, 550, 0, 0))

    # X=6500m, Y=2800m, HSpeed=-100m/s VSpeed=0m/s Fuel=600l, Angle=90°, Power=0
    def test_second_case(self):
        topo = Topo()
        topo.add([0, 100])
        topo.add([1000, 500])
        topo.add([1500, 100])
        topo.add([3000, 100])
        topo.add([3500, 500])
        topo.add([3700, 200])
        topo.add([5000, 1500])
        topo.add([5800, 300])
        topo.add([6000, 1000])
        topo.add([6999, 2000])
        topo.finalize()
        state = GameState(topo)
        state.update(6500, 2800, -100, 0, 600, 90, 0)

    def test_shorten_line(self):
        line = Line(Point(0,0), Point(1,1))
        line2 = line.shorten()
        self.assertAlmostEqual(line2.start.x, 0.05, delta=0.05)
        self.assertAlmostEqual(line2.start.y, 0.05, delta=0.05)
        self.assertAlmostEqual(line2.end.x, 0.95, delta=0.05)
        self.assertAlmostEqual(line2.end.y, 0.95, delta=0.05)
        line = Line(Point(0, 1), Point(1, 0))
        line2 = line.shorten()
        self.assertAlmostEqual(line2.start.x, 0.05, delta=0.05)
        self.assertAlmostEqual(line2.start.y, 0.95, delta=0.05)
        self.assertAlmostEqual(line2.end.x, 0.95, delta=0.05)
        self.assertAlmostEqual(line2.end.y, 0.05, delta=0.05)
        line = Line(Point(1, 0), Point(0, 1))
        line2 = line.shorten()
        self.assertAlmostEqual(line2.start.x, 0.95, delta=0.05)
        self.assertAlmostEqual(line2.start.y, 0.05, delta=0.05)
        self.assertAlmostEqual(line2.end.x, 0.05, delta=0.05)
        self.assertAlmostEqual(line2.end.y, 0.95, delta=0.05)

    def test_shorten_line_flat(self):
        line = Line(Point(0,0), Point(0,1))
        line2 = line.shorten()
        self.assertEqual(line2.start.x, 0)
        self.assertAlmostEqual(line2.start.y, 0.05, delta=0.05)
        self.assertEqual(line2.end.x, 0)
        self.assertAlmostEqual(line2.end.y, 0.95, delta=0.05)
        line = Line(Point(0, 0), Point(1, 0))
        line2 = line.shorten()
        self.assertEqual(line2.start.y, 0)
        self.assertAlmostEqual(line2.start.x, 0.05, delta=0.05)
        self.assertEqual(line2.end.y, 0)
        self.assertAlmostEqual(line2.end.x, 0.95, delta=0.05)

    def test_is_in_wall(self):
        topo = Topo()
        topo.add([0, 100])
        topo.add([1000, 500])
        topo.add([1500, 100])
        topo.add([3000, 100])
        topo.add([3500, 500])
        topo.add([3700, 200])
        topo.add([5000, 1500])
        topo.add([5800, 300])
        topo.add([6000, 1000])
        topo.add([6999, 2000])
        topo.finalize()
        self.assertTrue(topo.is_in_wall(Point(0,0)))
        self.assertFalse(topo.is_in_wall(Point(2000,2000)))

    def test_first_case(self):
        topo = self.first_case_topo()
        state = GameState(topo)
        state.update(2500, 2700, 0, 0, 550, 0, 0)

    def first_case_topo(self):
        topo = Topo()
        topo.add([0, 100])
        topo.add([1000, 500])
        topo.add([1500, 1500])
        topo.add([3000, 1000])
        topo.add([4000, 150])
        topo.add([5500, 150])
        topo.add([6999, 800])
        return topo

    def test_last_step(self):
        topo = self.first_case_topo()
        state = GameState(topo)
        state.update(5243, 186, 7, -40, 16, 2, 4)
        print(state.get_best_solution())
