import sys
import math
from datetime import timedelta

def transform(t):
    l = t.split(':')
    return timedelta(hours=int(l[0]), minutes=int(l[1]), seconds=int(l[2]))

l = transform("00:00:18")
n = 2
ts = ["00:00:05", "00:00:15"]
t0 = transform("00:00:00")
result = t0
for t in ts:
    result += transform(t)

print(result <= l)