import datetime
import sys
import math
import random

TIME_LIMIT = datetime.timedelta(microseconds=90000)

OTHER_SPECIMEN_NUMBER = 5

BEST_SPECIMEN_NUMBER = 5

CROSSOVER_SPLIT = 5

ELITE_CLONES = 25

POPULATION_SIZE = 25

CHROM_SIZE = 50
width = 7000
height = 3000


class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.distance = math.inf
        self.neighbours = []

    def round_eq(self, other):
        return int(round(self.x)) == int(round(other.x)) \
               and int(round(self.y)) == int(round(other.y))

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __repr__(self):
        return "%f,%f" % (self.x, self.y)

    def __sub__(self, other):
        return Vector(self.x - other.x, self.y - other.y)

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def is_out_of_bounds(self):
        if self.x < 0 or self.x > width or self.y < 0 or self.y > height:
            return True
        return False

    def dist(self, other):
        return math.sqrt((self.x-other.x)**2 + (self.y-other.y)**2)


class Vector(Point):
    def __mul__(self, other):
        return self.x * other.y - self.y * other.x


class TopoPoint(Point):
    def __init__(self, x, y, ratio_on_line):
        self.x = x
        self.y = y
        self.ratio = ratio_on_line
        self.distance = None


class Line(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end
        self.max_x = max(start.x, end.x)
        self.max_y = max(start.y, end.y)
        self.min_x = min(start.x, end.x)
        self.min_y = min(start.y, end.y)

    def crossing_point(self, other):
        if other.max_x < self.min_x or other.max_y < self.min_y or other.min_x > self.max_x or other.min_y > self.max_y:
            return None
        if other.start == self.start or other.start == self.end:
            return TopoPoint(other.start.x, other.start.y, ratio_on_line=0)
        if other.end == self.start or other.end == self.end:
            return TopoPoint(other.end.x, other.end.y, ratio_on_line=1)
        r = Vector(self.end.x - self.start.x, self.end.y - self.start.y)
        s = Vector(other.end.x - other.start.x, other.end.y - other.start.y)
        product_rs = r * s
        if product_rs != 0:
            Vsp = Vector(self.start.x - other.start.x, self.start.y - other.start.y)
            product_vr = Vsp * r
            t = -product_vr / product_rs
            product_vs = s * Vsp
            u = product_vs / product_rs
            if 0 <= t <= 1 and 0 <= u <= 1:
                return TopoPoint(self.start.x + u * r.x, self.start.y + u * r.y, u)
        return None

    def size(self):
        return math.sqrt((self.start.x - self.end.x) ** 2 + (self.start.y - self.end.y) ** 2)

    def shorten(self):
        start = Point(self.start.x + (self.end.x - self.start.x) * 0.0001, self.start.y + (self.end.y - self.start.y) *0.0001)
        end = Point(self.end.x + (self.start.x - self.end.x) * 0.0001, self.end.y + (self.start.y - self.end.y) * 0.0001)
        return Line(start, end)

    def __repr__(self):
        return "start: %s, end: %s" % (self.start, self.end)


class Command(object):
    def __init__(self, angle, power):
        self.angle = angle
        self.power = power

    def __repr__(self):
        return "angle: %d, power: %d" % (self.angle, self.power)


class RandomCommand(Command):
    def __init__(self):
        self.angle = random.randint(-15, 15)
        self.power = min(1, random.randint(-1, 5))


class Topo(object):
    def __init__(self):
        self.points = []
        self.lines = []
        self.obective_y = None
        self.land_line = None

    def add(self, coordinates):
        self.points.append(Point(coordinates[0], coordinates[1]))

    def check_collision(self, line):
        for topo_line in self.lines:
            point = topo_line.crossing_point(line)
            if point:
                point.distance = self.crash_distance_to_landing_site(topo_line, point.ratio)
                return point
        return None

    def check_collision_number(self, line):
        number = 0
        for topo_line in self.lines:
            point = topo_line.crossing_point(line)
            if point:
                number += 1
        return number

    def finalize(self):
        for i in range(len(self.points) - 1):
            point1 = self.points[i]
            point2 = self.points[i + 1]
            if point1.y == point2.y:
                self.obective_y = point2.y
                self.land_line = Line(point1, point2)
                point1.distance = 0
                point2.distance = 0
            self.lines.append(Line(point1, point2))
        self.define_nodes_distances()

    def define_nodes_distances(self):
        nodes_classified = []
        i=0
        while i<len(self.points):
            self.points = sorted(self.points, key=lambda x: x.distance)
            point = self.points[i]
            for tmp_point in self.points[i:]:
                shortened = Line(tmp_point, point).shorten()
                if not self.check_collision(shortened):
                    if not self.is_in_wall(shortened.start):
                        point.neighbours.append(tmp_point)
                        tmp_point.neighbours.append(point)
                        tmp_point.distance = min(tmp_point.distance, point.distance+point.dist(tmp_point))
            i+=1

    def crash_distance_to_landing_site(self, topo_line, ratio):
        index = self.lines.index(topo_line)
        line_iter = self.lines[index]
        if line_iter.start.y == line_iter.end.y:
            return 0
        distances = []
        distance1 = -line_iter.size() * ratio
        while line_iter.start.y != line_iter.end.y and index < len(self.lines) - 1:
            distance1 += line_iter.size()
            index += 1
            line_iter = self.lines[index]
        if line_iter.start.y == line_iter.end.y:
            distances.append(distance1)

        index = self.lines.index(topo_line)
        line_iter = self.lines[index]
        distance2 = -line_iter.size() * (1 - ratio)
        while line_iter.start.y != line_iter.end.y and index > 0:
            distance2 += line_iter.size()
            index -= 1
            line_iter = self.lines[index]
        if line_iter.start.y == line_iter.end.y:
            distances.append(distance2)
        return min(distances)

    def distance_to_land(self, position):
        if self.land_line.start.x>position.x>self.land_line.end.x or self.land_line.start.x<position.x<self.land_line.end.x:
            if not self.check_collision(Line(position, Point(position.x, self.obective_y)).shorten()):
                return position.y - self.obective_y
        if not self.check_collision(Line(position, self.land_line.start).shorten()) and not self.check_collision(Line(position, self.land_line.end).shorten()):
            return min(position.dist(self.land_line.start), position.dist(self.land_line.end))
        distances = []
        for point in self.points:
            if not distances or min(distances)<point.distance:
                if not self.check_collision(Line(position, point).shorten()):
                    distances.append(point.distance + position.dist(point))
        return min(distances)

    def is_in_wall(self, point):
        return self.check_collision_number(Line(point, Point(width/2, height))) % 2 == 1


class GameState(object):
    def __init__(self, topo):
        self.topo = topo
        self.x = None
        self.y = None
        self.hs = None
        self.vs = None
        self.f = None
        self.r = None
        self.p = None
        self.population = None

    def update(self, x, y, hs, vs, f, r, p):
        self.x = x
        self.y = y
        self.hs = hs
        self.vs = vs
        self.f = f
        self.r = r
        self.p = p

    def get_best_solution(self):
        start_date = datetime.datetime.now()
        self.create_population()
        generations = 0
        print("start_date:", start_date, file=sys.stderr)
        while datetime.datetime.now() - start_date < TIME_LIMIT:
            self.generation()
            generations += 1
        print("Number of generations:", generations, file=sys.stderr)
        print("in", datetime.datetime.now() - start_date, file=sys.stderr)
        self.population = sorted(self.population, key=lambda x: x.get_score(self.topo))
        print([pop.get_score(self.topo) for pop in self.population], file=sys.stderr)
        best_sol = self.population[0]
        print("Best score:", best_sol.get_score(self.topo), file=sys.stderr)
        command = best_sol.commands[0]
        tmp_shuttle = Shuttle(self.x, self.y, self.hs, self.vs, self.f, self.r, self.p)
        tmp_shuttle.next(command.angle, command.power)
        self.update(tmp_shuttle.position.x, tmp_shuttle.position.y, tmp_shuttle.hs, tmp_shuttle.vs, tmp_shuttle.fuel, tmp_shuttle.rot, tmp_shuttle.power)
        return "%d %d" % (self.r, self.p)

    def create_population(self):
        if self.population:
            self.population = self.population[:ELITE_CLONES]
            for shut in self.population:
                del shut.commands[0]
                shut.commands.append(RandomCommand())
                shut.score = None
        else:
            self.population = []
        while len(self.population)<POPULATION_SIZE:
            tmp_shuttle = Shuttle(self.x, self.y, self.hs, self.vs, self.f, self.r, self.p)
            tmp_shuttle.generate_random_commands()
            self.population.append(tmp_shuttle)

    def selection(self):
        sorted_list = sorted(self.population, key=lambda x: x.get_score(self.topo), reverse=False)
        output = sorted_list[:BEST_SPECIMEN_NUMBER]
        #  * Randomly select other individuals
        rest = sorted_list[BEST_SPECIMEN_NUMBER:]
        random.shuffle(rest)
        output.extend(rest[:OTHER_SPECIMEN_NUMBER])
        self.population = output

    def crossover(self, parent1, parent2):
        split = random.randint(0, CHROM_SIZE-1)
        half_parent1 = parent1.commands[:split]
        half_parent2 = parent2.commands[split:]
        child = Shuttle(self.x, self.y, self.hs, self.vs, self.f, self.r, self.p)
        child.commands = half_parent1 + half_parent2
        self.mutation(child)
        return child

    def mutation(self, shuttle):
        i = random.randint(0, CHROM_SIZE-1)
        #  * Random gene mutation : a character is replaced
        shuttle.commands[i] = RandomCommand()

    def generation(self):
        self.selection()
        children = [self.population[0]]
        while len(children) < POPULATION_SIZE:
            parent1 = random.choice(self.population)  # randomly selected
            parent2 = random.choice(self.population)  # randomly selected
            if parent1 != parent2:
                children.append(self.crossover(parent1, parent2))
                children.append(self.crossover(parent1, parent2))
        # return the new generation
        self.population = children

    def __repr__(self):
        return "%f, %f, %d, %d, %d, %d, %d" % (self.x, self.y, self.hs, self.vs, self.f, self.r, self.p)


class Shuttle(object):
    def __init__(self, x, y, hs, vs, f, r, p):
        self.position = Point(x, y)
        self.hs = hs
        self.vs = vs
        self.fuel = f
        self.rot = r
        self.power = p
        self.commands = []
        self.score = None

    def next(self, angle_change, power_change):
        self.rot = max(min(self.rot + angle_change, 90), -90)
        self.power = max(min(self.power + power_change, 4), 0)
        rad_rot = -self.rot * math.pi / 180
        nhs = self.hs + self.power * math.sin(rad_rot)
        nvs = self.vs - 3.711 + self.power * math.cos(rad_rot)
        self.position.x += (nhs + self.hs) / 2
        self.position.y += (nvs + self.vs) / 2
        self.hs = nhs
        self.vs = nvs
        self.fuel -= self.power

    def generate_random_commands(self):
        while len(self.commands) < CHROM_SIZE:
            self.commands.append(RandomCommand())

    def get_score(self, topology):
        if self.score is None:
            self.calculate_score(topology)
        return self.score

    def calculate_score_crash(self, topology):
        for command in self.commands:
            previous_position = Point(self.position.x, self.position.y)
            self.next(command.angle, command.power)
            crashing_site = topology.check_collision(Line(previous_position, self.position))
            if crashing_site:
                if crashing_site.distance != 0:
                    self.score = crashing_site.distance
                    return
                self.score = -1000 + max(abs(self.hs), 15) + max(abs(self.vs), 20) * 10 + abs(self.rot)
                return
            elif self.position.is_out_of_bounds():
                self.score = 1000000 - abs(self.hs) - abs(self.vs)
                return
        self.score = 1000

    def calculate_score(self, topology):
        for command in self.commands:
            previous_position = Point(self.position.x, self.position.y)
            self.next(command.angle, command.power)
            crashing_site = topology.check_collision(Line(previous_position, self.position))
            if crashing_site:
                if crashing_site.distance != 0:
                    self.score = 10000 + crashing_site.distance
                    return
                if abs(self.hs) > 20 or abs(self.vs) > 40:
                    self.score = 1000 + max(abs(self.hs), 20) + max(abs(self.vs), 40)
                    return
                self.score = -100 + abs(self.rot)
                return
            elif self.position.is_out_of_bounds():
                self.score = 100000 + abs(self.hs) + abs(self.vs)
                return
        distance = topology.distance_to_land(self.position)
        if distance>0:
            self.score = distance
            return
        self.score = -1000 + max(abs(self.vs), 40) + max(abs(self.hs), 20) + abs(self.rot)

    def __eq__(self, other):
        return self.position.round_eq(other.position) \
               and int(round(self.hs)) == int(round(other.hs)) \
               and int(round(self.vs)) == int(round(other.vs)) \
               and self.fuel == other.fuel \
               and self.rot == other.rot \
               and self.power == other.power

    def __cmp__(self, other):
        return self.__eq__(other)

    def __repr__(self):
        return "x: %d, y: %d, hs: %d, vs: %d, f: %d, r: %d, p: %d" % (
            int(round(self.position.x)), int(round(self.position.y)),
            int(round(self.hs)), int(round(self.vs)),
            self.fuel, self.rot, self.power)


if __name__ == '__main__':
    # Save the Planet.
    # Use less Fossil Fuel.
    n = int(input())  # the number of points used to draw the surface of Mars.
    topo = Topo()
    for i in range(n):
        # land_x: X coordinate of a surface point. (0 to 6999)
        # land_y: Y coordinate of a surface point. By linking all the points together in a sequential fashion, you form the surface of Mars.
        inp = input()
        point = [int(j) for j in inp.split()]
        print(", ".join(inp.split()), file=sys.stderr)
        topo.add(point)
    topo.finalize()
    state = None
    # game loop
    while True:
        # hs: the horizontal speed (in m/s), can be negative.
        # vs: the vertical speed (in m/s), can be negative.
        # f: the quantity of remaining fuel in liters.
        # r: the rotation angle in degrees (-90 to 90).
        # p: the thrust power (0 to 4).
        inp = input()
        x, y, hs, vs, f, r, p = [int(i) for i in inp.split()]
        print(", ".join(inp.split()), file=sys.stderr)
        if not state:
            state = GameState(topo)
            state.update(x, y, hs, vs, f, r, p)
        else:
            print(state, file=sys.stderr)
        # Write an action using print
        # To debug: print("Debug messages...", file=sys.stderr)
        print(state.get_best_solution())
