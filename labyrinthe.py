import sys
import math
import hashlib

w, h = [int(i) for i in input().split()]
x, y = [int(i) for i in input().split()]
labyrinthe = [[x for x in input()] for _ in range(h)]

class Point():
    
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.val = labyrinthe[y][x]
        
    def __bool__(self):
        return self.val == "."
        
    def __repr__(self):
        return '{} {}'.format(self.x, self.y)
        
   def __lt__(self, other):
        if self.x == other.x:
            return self.y < other.y
        else:
            return self.x < other.x
        
    def __eq__(self, other):
        return self.x == other.x and self.y == other.y
        
    def is_exit(self):
        return ((self.x == 0 or self.x == w -1)
                or (self.y == 0 or self.y == h -1))
                and self
        
    def neighboors(self):
        min_x = 0 if self.x > 0 else 1
        max_x = 3 if self.x < w-1 else 2
        min_y = 0 if self.y > 0 else 1
        max_y = 3 if self.y < h-1 else 2
        for i in range(min_x, max_x):
            for j in range(min_y, max_y):
                yield Point(self.x - 1+ i, self.y -1 +j)
                
    def valid_neighboors(self):
        for neighboor in self.neighboors():
            if neighboor:
                yield neighboor

start = Point(x, y)
visited= [start]

print(list(start.valid_neighboors()), file=sys.stderr)
print(list(Point(5,2).valid_neighboors()), file=sys.stderr)
print(list(Point(5,3).valid_neighboors()), file=sys.stderr)

def visit(p):
    for n in p.valid_neighboors():
        if n not in visited:
            visited.append(n)
            visit(n)

visit(start)
results = sorted([p for p in visited if p.is_exit()])

print (len(results))
for r in results:
    print(r)