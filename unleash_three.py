import sys
import math
from collections import deque


class Graph(object):
    def __init__(self, carto):
        self.carto = carto
        self.distances = {}

    def bfs_paths(self, start, goal):
        queue = deque([(start, [start])])
        while queue:
            (vertex, path) = queue.popleft()
            for next in self.carto[vertex] - set(path):
                if next == goal:
                    yield path + [next]
                else:
                    queue.append((next, path + [next]))

    def shortest_path(self, start, goal):
        try:
            return next(self.bfs_paths(start, goal))
        except StopIteration:
            return []

    def all_shortest_paths(self, start, goal):
        try:
            generator = self.bfs_paths(start, goal)
            shortest_path = next(generator)
            next_path = shortest_path
            output = []
            while len(next_path)==len(shortest_path):
                output.append(next_path)
                next_path = next(generator)
            return output
        except StopIteration:
            return []

    def get_closest_points(self, nodes, target):
        min_dist=10000
        output = []
        for node in nodes:
            distance = self.dist(node,target)
            print("distance", distance, node, target, file=sys.stderr)
            if distance<min_dist:
                min_dist = distance
                output = [node]
            elif distance==min_dist:
                output.append(node)
        return output



    def dist_two_list(self, nodesA, nodesB):
        min_dist = 10000
        for start in nodesA:
            dist = self.dist_list(start, nodesB)
            if min_dist > dist:
                min_dist = dist
        return min_dist

    def dist_list(self, nodeA, nodes):
        min_dist = 10000
        for target in nodes:
            dist = self.dist(nodeA, target)
            if min_dist > dist:
                min_dist = dist
        return min_dist

    def dist2(self, nodeA, nodeB):
        if (nodeA.id, nodeB.id) not in self.distances:
            self.distances[(nodeA.id, nodeB.id)] = len(self.shortest_path(nodeA.id, nodeB.id))
            self.distances[(nodeB.id, nodeA.id)] = self.distances[(nodeA.id, nodeB.id)]
        return self.distances[(nodeA.id, nodeB.id)]

    def dist(self, nodeA, nodeB):
        if (nodeA.id, nodeB.id) not in self.distances:
            self.distances[(nodeA.id, nodeB.id)] = self.calculate_distance(nodeA, nodeB)
            self.distances[(nodeB.id, nodeA.id)] = self.distances[(nodeA.id, nodeB.id)]
        return self.distances[(nodeA.id, nodeB.id)]

    def calculate_distance(self, nodeA, nodeB):
        nodes = {nodeA.id: 0}
        dist = 0
        while nodeB.id not in nodes:
            dist += 1
            next_nodes = {}
            for node in nodes:
                for next_node in self.carto[node]:
                    if next_node not in nodes:
                        next_nodes[next_node] = dist
            nodes.update(next_nodes)
        nodes2 = {nodeB.id: 0}
        dist2 = 0
        while nodeA.id not in nodes2:
            dist2 += 1
            next_nodes = {}
            for node in nodes2:
                for next_node in self.carto[node]:
                    if next_node not in nodes2:
                        next_nodes[next_node] = dist2
            nodes2.update(next_nodes)
        total_nodes = {}
        for node in nodes:
            total_nodes[node] = nodes[node] + nodes2.get(node, 1000)
        path = []
        for node in total_nodes:
            if total_nodes[node] == total_nodes[nodeB.id]:
                path.append(node)
        output_nodes = []
        for node in path:
            if nodes[node] == 1:
                output_nodes.append(node)
        return nodes[nodeB.id]

    def find_critical_points(self, nodeA, nodeB):
        far_values = {}
        for node_id in self.carto:
            node = Node(node_id, 0, 0, 0, 0, True)
            far_values[node] = self.dist(nodeA, node) + self.dist(nodeB, node)
        far_node = max(far_values, key=lambda key: far_values[key])
        paths1 = self.all_shortest_paths(nodeA.id, far_node.id)
        paths2 = self.all_shortest_paths(nodeB.id, far_node.id)
        points = self.points_of_convergence(paths1[0], paths2[0])
        for path1 in paths1:
            for path2 in paths2:
                points = points.intersection(self.points_of_convergence(path1, path2))
        return points

    def points_of_convergence(self, path1, path2):
        points = set()
        for i in range(min(len(path1),len(path2)) - 1):
            if path1[i] == path2[i]:
                points.add(path1[i])
        return points


class Node(object):
    def __init__(self, id, my_influence, my_tolerance, other_influence, other_tolerance, can_influence):
        self.id = id
        self.my_influence = my_influence
        self.my_tolerance = my_tolerance
        self.other_influence = other_influence
        self.other_tolerance = other_tolerance
        self.can_influence = can_influence
        self.owner = 0
        if self.my_influence > self.other_influence:
            self.owner = 1
        elif self.my_influence < self.other_influence:
            self.owner = -1
        self.priority = self.attack_priority()
        self.value = None
        self.can_enemy_influence = False
        self.neighbors = []
        self.enemies = 0
        self.copains = 0

    def attack_priority(self):
        if self.can_influence:
            if self.owner == -1:
                return 1
            elif self.owner == 0:
                return 2
            return 3
        return 0

    def can_sacrifice(self):
        return self.my_influence > 5

    def __repr__(self):
        return str(self.id)


class Game(object):
    def __init__(self, graph, center_values, contention_points):
        self.node_list = []
        self.node_dict = {}
        self.center_values = center_values
        self.graph = graph
        self.our_nodes = []
        self.enemy_nodes = []
        self.solution = Solution()
        self.contention_points = contention_points

    def add_node(self, node):
        self.node_list.append(node)
        self.node_dict[node.id] = node

    def compute_center_values(self):
        for node in self.node_list:
            center_value = 0
            for node2 in self.node_list:
                center_value += self.graph.dist(node, node2)
            self.center_values[node.id] = float(center_value) / len(self.node_list)

        print("center", [self.center_values[x] for x in sorted(self.center_values, key=lambda k: self.center_values[k])],
              file=sys.stderr)

    def define_nodes(self):
        for node in self.node_list:
            if node.owner == 1:
                self.our_nodes.append(node)
            elif node.owner == -1:
                self.enemy_nodes.append(node)

        if type(self.contention_points) != type(set()):
            self.contention_points = set()
            if len(self.node_list) < 40:
                self.contention_points = self.graph.find_critical_points(self.our_nodes[0], self.enemy_nodes[0])
        return self.contention_points

    def find_best_nodes(self):
        min_dist = 10000
        potential_nodes = []
        self.define_nodes()
        for mynode in self.our_nodes:
            for enemynode in self.enemy_nodes:
                path = self.graph.dijkstra(self.graph.get_vertex(mynode.id), self.graph.get_vertex(enemynode.id))
                dist = len(path) - 1
                closer_nodes_ids = path[-2] if len(path) >= -2 else enemynode.id
                if dist == min_dist:
                    potential_nodes.extend(closer_nodes_ids)
                elif dist < min_dist:
                    potential_nodes = closer_nodes_ids[:]
                    min_dist = dist
        return potential_nodes

    def compute_value(self):
        for node in self.node_list:
            node.value = self.graph.dist_list(node, self.enemy_nodes) - self.graph.dist_list(node, self.our_nodes)

    def compute_influence(self):
        for node in self.node_list:
            for neigh in node.neighbors:
                if self.node_dict[neigh].owner == -1:
                    node.can_enemy_influence = True
                    node.enemies += 1
                elif self.node_dict[neigh].owner == 1:
                    node.copains += 1

    def compute_solution(self, turns_left, main_contention):
        moves_left = 5
        if self.contention_points:
            own_contention = False
            for point in self.contention_points:
                for node in self.our_nodes:
                    if point == node.id:
                        own_contention = True
                        if not main_contention:
                            main_contention = node
            if turns_left<planet_count-2:
                print("self.contention_points", self.contention_points, file=sys.stderr)
                print("self.main_contention", main_contention, file=sys.stderr)
            main_contention_node = main_contention
            if own_contention:

                secured = (main_contention_node.my_influence > main_contention_node.other_influence + 6) or (main_contention_node.other_tolerance == 0 or main_contention_node.my_tolerance == 0)
                print("secured", secured,main_contention_node.my_influence, main_contention_node.other_influence , file=sys.stderr)
                if secured:
                    moves_left = self.dwarf_fortress(moves_left)
                    self.vae_victis(moves_left)
                else:
                    self.lockdown(main_contention_node)
            elif self.graph.dist_two_list(self.our_nodes, self.enemy_nodes) > 4:
                self.blitzkrieg(self.contention_points)
                moves_left = 0
            elif not main_contention:
                moves_left = self.ghost_division(self.contention_points)
            elif (main_contention_node.owner == 0 ) and (main_contention_node.other_tolerance == 0 and main_contention_node.my_tolerance == 0):
                value = 0
                saved_pote = None
                for node_id in main_contention_node.neighbors:
                    node = self.node_dict[node_id]
                    value += node.owner
                    if node.owner == 1:
                        saved_pote = node
                if value >= 0:
                    self.solution.enrich_solution(saved_pote, 5)
                    self.solution.sacr(saved_pote)
                else :
                    self.vae_victis(moves_left)
            else:
                self.vae_victis(moves_left)
            # if turns_left < planet_count - 8:
            #     contention_points = set(main_contention)
        elif turns_left < 5:
            moves_left = self.last_turns(moves_left)
            self.vae_victis(moves_left)
        elif self.graph.dist_two_list(self.our_nodes, self.enemy_nodes) > 4:
            self.blitzkrieg()
        elif self.graph.dist_two_list(self.our_nodes, self.enemy_nodes) <= 2:
            moves_left = self.dwarf_fortress(moves_left)
            self.vae_victis(moves_left)
        self.vae_victis(moves_left)
        self.solution.display()
        return main_contention

    def vae_victis(self, moves_left):
        moves_left = self.dobbermove(moves_left, False)
        moves_left = self.defensive_move(moves_left)
        moves_left = self.dobbermove(moves_left, True)
        if moves_left == 5:
            self.passive_aggressive_move(moves_left)

    def defensive_move(self, moves_left):
        print("defensive_move", moves_left, file=sys.stderr)
        for node in self.node_list:
            if node.can_enemy_influence and node.owner == 1:
                number = node.other_influence - node.my_influence + 5
                number = moves_left
                # TODO count neighors friendly and enemy
                if number > 0 and node.other_tolerance > 0 and moves_left > 0:
                    self.solution.enrich_solution(node, number)
                    moves_left -= number
        return moves_left

    def dobbermove(self, moves_left, discriminant):
        if moves_left <= 0:
            return 0
        print("dobbermove", moves_left, file=sys.stderr)
        invasion_list = []
        for node in self.node_list:
            if node.priority == 1 and (node.copains >= node.enemies or discriminant):
                invasion_list.append(node)
        if len(invasion_list) > 0:
            print("invasion_list on ", invasion_list, file=sys.stderr)
            invasion_list = sorted(invasion_list, key=lambda k: self.center_values[k.id])
            print("aggression on ", invasion_list[0], file=sys.stderr)
            self.solution.enrich_solution(invasion_list[0], moves_left)
            return 0
        else:
            for node in self.node_list:
                if node.priority == 2 and (node.copains >= node.enemies or discriminant):
                    if node.value <= 0 or node.id not in self.solution.output_nodes:
                        invasion_list.append(node)
        invasion_list = sorted(invasion_list, key=lambda k: k.value)
        print("invasion_list", invasion_list, file=sys.stderr)
        if invasion_list:
            while len(invasion_list) <= moves_left:
                invasion_list.append(invasion_list[0])
            self.solution.define(invasion_list)
            moves_left = 0
        return moves_left

    def passive_aggressive_move(self, moves_left):
        print("passive_aggressive_move", moves_left, file=sys.stderr)
        node_dist = 10000
        explonode = None
        for node in self.node_list:
            if node.priority == 3 and moves_left > 0:
                distance = self.graph.dist_list(node, self.enemy_nodes)
                if distance < node_dist:
                    node_dist = distance
                    explonode = node
        number = moves_left
        self.solution.enrich_solution(explonode, number)
        self.solution.sacr(explonode)
        moves_left -= number
        return moves_left

    def blitzkrieg(self, points=None):
        if not points:
            invasion_list = []
            for node in self.node_list:
                if node.priority == 1:
                    invasion_list.append(node)
            if len(invasion_list) == 0:
                for node in self.node_list:
                    if node.priority == 2:
                        invasion_list.append(node)
            invasion_list = sorted(invasion_list, key=lambda k: (k.value, self.center_values[k.id]))
        else:
            objective_id = points.pop()
            objective_node = Node(objective_id, 0, 0, 0, 0, True)
            print("self.our_nodes", self.our_nodes, file=sys.stderr)
            closest_points = self.graph.get_closest_points(self.our_nodes, objective_node)
            invasion_list = []
            for close in closest_points:
                for path in self.graph.all_shortest_paths(close.id, objective_node.id):
                    node = self.node_dict[path[1]]
                    invasion_list.append(node)
            invasion_list = sorted(invasion_list, key=lambda k: k.neighbors, reverse=True)
        if invasion_list:
            node = invasion_list[0]
            self.solution.enrich_solution(node, 5)
            self.solution.sacr(node)

    def last_turns(self, moves_left):
        invasion_list = []
        for node in self.node_list:
            if node.priority == 2:
                invasion_list.append(node)
        invasion_list = sorted(invasion_list, key=lambda k: k.value)
        self.solution.define(invasion_list)
        moves_left -= len(invasion_list)
        return moves_left

    def dwarf_fortress(self, moves_left):
        print("dwarf_fortress", moves_left, file=sys.stderr)
        fortress_list = []
        for node in self.node_list:
            distance = self.graph.dist_list(node, self.enemy_nodes)
            if node.can_influence and node.owner == 0 and 4 > distance >= 2:
                fortress_list.append((node, distance))
        fortress_list = sorted(fortress_list, key=lambda k: k[1])
        for tuple in fortress_list:
            self.solution.enrich_solution(tuple[0], 1)
            moves_left -= 1
        return moves_left

    def ghost_division(self, contention_points):
        print("through the gates of hell", contention_points, file=sys.stderr)
        objective_id = list(contention_points)[0]
        objective_node = Node(objective_id, 0, 0, 0, 0, True)
        print("self.our_nodes", self.our_nodes, file=sys.stderr)
        print("objective_node", objective_node, file=sys.stderr)
        closest_points = self.graph.get_closest_points(self.our_nodes, objective_node)
        print("closest_points", closest_points, file=sys.stderr)
        invasion_list = []
        for close in closest_points:
            for path in self.graph.all_shortest_paths(close.id, objective_node.id):
                print("path", path, file=sys.stderr)
                node = self.node_dict[path[1]]
                invasion_list.append(node)
        invasion_list = sorted(invasion_list, key=lambda k: (k.value, self.center_values[k.id]))
        print("invasion_list", invasion_list, file=sys.stderr)
        if invasion_list:
            main_contention_node = invasion_list[0]
            if (main_contention_node.owner == 0) and (main_contention_node.other_tolerance == 0 and main_contention_node.my_tolerance == 0):
                value = 0
                saved_pote = None
                for node_id in main_contention_node.neighbors:
                    node = self.node_dict[node_id]
                    value += node.owner
                    if node.owner == 1:
                        saved_pote = node
                if value >= 0:
                    self.solution.enrich_solution(saved_pote, 5)
                    self.solution.sacr(saved_pote)
                else:
                    self.vae_victis(5)
            else:
                node = invasion_list[0]
                self.solution.enrich_solution(node, 5)
                return 0
        return 5

    def lockdown(self, mcp):
        print("locking down", mcp, file=sys.stderr)
        self.solution.enrich_solution(mcp, 4)
        for node in self.node_list:
            distance = self.graph.dist_list(node, self.enemy_nodes)
            if node.can_influence and node.owner == 0 and 4 > distance >= 2 and node.id in mcp.neighbors:
                self.solution.enrich_solution(node, 1)
                return
        self.solution.enrich_solution(mcp, 1)



class Solution(object):
    def __init__(self):
        self.output_nodes = []
        self.sacrifice = "NONE"

    def enrich_solution(self, node, number=1):
        for i in range(number):
            self.output_nodes.append(node.id)

    def sacr(self, node):
        self.sacrifice = node.id

    def display(self):
        output_list = self.output_nodes[:5]
        while len(output_list) < 5:
            if len(self.output_nodes) > 0:
                print("filling with", self.output_nodes[0], file=sys.stderr)
                output_list.append(self.output_nodes[0])
            else:
                output_list.append(0)
        for i in output_list:
            print(i)
        print(self.sacrifice)

    def define(self, invasion_list):
        for planet in invasion_list:
            self.output_nodes.append(planet.id)

def ut_setup():
    carto = {}
    carto[1] = set([2, 3, 4])
    carto[2] = set([1, 3])
    carto[3] = set([1, 2, 4, 5])
    carto[4] = set([1, 3])
    carto[5] = set([3, 6, 7, 8])
    carto[6] = set([5, 7, 8])
    carto[7] = set([5, 6, 8])
    carto[8] = set([5, 6, 7])

    graph = Graph(carto)
    nodeA = Node(2, 0, 0, 0, 0, True)
    nodeB = Node(4, 0, 0, 0, 0, True)
    return graph, nodeA, nodeB

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.
if __name__ == '__main__':
    planet_count, edge_count = [int(i) for i in input().split()]

    cartographie = {}
    for i in range(edge_count):
        node_1, node_2 = [int(j) for j in input().split()]
        if not node_1 in cartographie:
            cartographie[node_1] = set()
        if not node_2 in cartographie:
            cartographie[node_2] = set()
        cartographie[node_1].add(node_2)
        cartographie[node_2].add(node_1)

    graph = Graph(cartographie)
    center_values = {}
    # game loop
    turns_left = planet_count
    contention_points = None
    main_contention= None
    while True:
        game = Game(graph, center_values, contention_points)
        for i in range(planet_count):
            my_units, my_tolerance, other_units, other_tolerance, can_assign = [int(j) for j in input().split()]
            node = Node(i, my_units, my_tolerance, other_units, other_tolerance, can_assign)
            node.neighbors = cartographie[i]
            game.add_node(node)
        contention_points = game.define_nodes()
        print("contention_points", contention_points, file=sys.stderr)
        game.compute_center_values()
        game.compute_influence()
        print("compute_value", file=sys.stderr)
        game.compute_value()
        print("compute_solution", file=sys.stderr)
        main_contention = game.compute_solution(turns_left, main_contention)
        if main_contention:
            contention_points = set([main_contention.id])
        turns_left -= 1
