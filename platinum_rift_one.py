import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

# player_count: the amount of players (2 to 4)
# my_id: my player ID (0, 1, 2 or 3)
# zone_count: the amount of zones on the map
# link_count: the amount of links between all zones
player_count, my_id, zone_count, link_count = [int(i) for i in input().split()]
platinum_map = {}
weigthed_platinum_map = {}
war_map = {}

for i in range(zone_count):
    # zone_id: this zone's ID (between 0 and zoneCount-1)
    # platinum_source: the amount of Platinum this zone can provide per game turn
    zone_id, platinum_source = [int(j) for j in input().split()]
    platinum_map[zone_id] = platinum_source

links = {}
for i in range(link_count):
    zone_1, zone_2 = [int(j) for j in input().split()]
    if not zone_1 in links:
        links[zone_1] = []
    if not zone_2 in links:
        links[zone_2] = []
    links[zone_1].append(zone_2)
    links[zone_2].append(zone_1)


def compute_continents():
    zone_list = list(platinum_map.keys())
    continents = []
    while len(zone_list) > 0:
        continent = []
        old_len = 0
        continent.append(zone_list.pop())
        while len(continent) > old_len:
            old_len = len(continent)
            for land in continent:
                for connected_land in links[land]:
                    if connected_land not in continent:
                        continent.append(connected_land)
                        zone_list.remove(connected_land)
        continents.append(continent)
    return continents


def compute_weigthed_platinum(continents, reverse=True):
    for cell in platinum_map:
        continent_size = 100
        for continent in continents:
            if cell in continent:
                continent_size = len(continent)
        if reverse:
            weigthed_platinum_map[cell] = float(platinum_map[cell]) / continent_size
        else:
            weigthed_platinum_map[cell] = float(platinum_map[cell]) * continent_size


def pods_near(cell):
    pods_near = []
    for near_cell in links[cell]:
        pods_near.append(war_map[near_cell]['pods'])
    result = [sum(x) for x in zip(*pods_near)]
    return result


def number_enemies_near(cell):
    pods = pods_near(cell)
    pods.pop(my_id)
    return sum(pods)


def max_enemies_near(cell):
    pods = pods_near(cell)
    pods.pop(my_id)
    return max(pods)


def allies_near(cell):
    pods = pods_near(cell)
    return pods.pop(my_id)


def enemy_territories_near(cell):
    result = 0
    cells = links[cell]
    for voisin in cells:
        if war_map[voisin]['owner'] not in [my_id, -1]:
            result += 1
    return result


def get_continents_sorted_by_ressources(continents):
    continents_nb = len(continents)
    ressources = []
    for continent in continents:
        ressource = 0
        for land in continent:
            ressources += platinum_map[land]
        ressources.append(ressource)
    ponderated_ressources = []
    for i in range(continents_nb):
        ponderated_ressources.append(ressources[i] / len(continents[i]))
    return sorted(range(continents_nb), key=lambda k: ponderated_ressources[k])


def way_to_closer_enemy(initial_cell):
    classified_cells = []
    next_voisins = [initial_cell]
    aim = None
    while (len(next_voisins) > 0 and aim is None):
        previous_voisins = next_voisins
        next_voisins = []
        for cell in previous_voisins:
            if aim:
                break
            for voisin in links[cell]:
                if voisin not in classified_cells:
                    classified_cells.append(voisin)
                    next_voisins.append(voisin)
                    if (war_map[voisin]['owner'] != my_id):
                        aim = voisin
                        break
    if aim is None:
        return None
    print("aim is ", aim, "property of", war_map[aim]['owner'], file=sys.stderr)
    classified_cells = []
    weigths = {aim: 0}
    weigth = 0
    next_voisins = [aim]
    while (len(next_voisins) > 0 and initial_cell not in classified_cells):
        weigth += 1
        previous_voisins = next_voisins
        next_voisins = []
        for cell in previous_voisins:
            for voisin in links[cell]:
                if voisin not in classified_cells:
                    weigths[voisin] = weigth
                    classified_cells.append(voisin)
                    next_voisins.append(voisin)
    min_weigth = weigths[initial_cell] - 1
    for cell in links[initial_cell]:
        if cell in weigths and weigths[cell] <= min_weigth:
            print("shortest way is", cell, file=sys.stderr)
            return cell
    return None


def priority_voisins(cell, troops):
    result = []
    rest_troops = troops
    voisins = links[cell]
    voisins_interessants = []
    prio1 = []
    prio2 = []
    for voisin in voisins:
        if war_map[voisin]['owner'] != my_id:
            voisins_interessants.append(voisin)
    if len(voisins_interessants) == 0:
        print("way_to_closer_enemy", file=sys.stderr)
        way = way_to_closer_enemy(cell)
        if way is not None:
            result.append((troops, way))
        return result
    if len(voisins_interessants) == 1:
        result.append((troops, voisins_interessants[0]))
        return result
    else:
        for voisin in voisins_interessants:
            if platinum_map[voisin] > 0:
                prio1.append((voisin, platinum_map[voisin]))
            else:
                prio2.append((voisin, max(war_map[voisin]['pods'])))
    prio1.sort
    for voisin, plat in sorted(prio1, key=lambda x: x[1], reverse=True):
        if rest_troops <= 0:
            break
        to_send = min(rest_troops, max(war_map[voisin]['pods']) + 1)
        rest_troops -= to_send
        result.append((to_send, voisin))
    for voisin, enemy_troops in sorted(prio2, key=lambda x: x[1]):
        if rest_troops <= 0:
            break
        to_send = min(rest_troops, enemy_troops + 1)
        rest_troops -= to_send
        result.append((to_send, voisin))
    if rest_troops > 0:
        modified = (result[0][0] + rest_troops, result[0][1])
        result[0] = modified
    return result


def best_move(cell):
    result = ""
    if war_map[cell]['pods'][my_id] == 0:
        return result
    to_move = war_map[cell]['pods'][my_id] - max_enemies_near(cell)
    if to_move <= 0:
        return result
    print("cell : ", cell, "has", war_map[cell]['pods'][my_id], "troops, and will move", to_move, file=sys.stderr)
    for number, voisin in priority_voisins(cell, to_move):
        result += str(number) + " " + str(cell) + " " + str(voisin) + " "
    return result


def best_buy(platinum):
    result = ""
    if player_count > 2:
        temp_plat = weigthed_platinum_map.copy()
    else:
        temp_plat = platinum_map.copy()
    while platinum > 19 and len(temp_plat) > 0:
        new_troops = 0
        zone = max(temp_plat, key=temp_plat.get)
        # print >> sys.stderr, "temp_plat -",zone, temp_plat[zone]
        if war_map[zone]['owner'] == my_id:
            diff = max_enemies_near(zone) - war_map[zone]['pods'][my_id]
            if diff > 0:
                new_troops = min(platinum / 20, diff)
        elif war_map[zone]['owner'] == -1:
            new_troops = int(platinum / (20 * len(temp_plat)))
            if new_troops == 0:
                new_troops = 1
        else:
            pass
        # print >> sys.stderr, "new_troops : ", new_troops, zone
        if new_troops != 0:
            result = result + str(new_troops) + " " + str(zone) + "  "
        del temp_plat[zone]
        platinum -= new_troops * 20
    if platinum > 19:
        potential_zones = []
        for zone in war_map:
            enemy_cells = enemy_territories_near(zone)
            if war_map[zone]['owner'] == my_id and enemy_cells > 0:
                potential_zones.append(zone)
        while platinum > 19:
            current_cell = potential_zones.pop(0)
            result = result + "1 " + str(current_cell) + "  "
            potential_zones.append(current_cell)
            platinum -= 20
    return result


continents = compute_continents()
compute_weigthed_platinum(continents, False)

turn = 0
# game loop
while 1:
    platinum = int(input())  # my available Platinum
    move = ""
    # if turn == 1:
    # compute_weigthed_platinum(continents, False)
    print("weigthed_platinum : ", weigthed_platinum_map, file=sys.stderr)
    for i in range(zone_count):
        # z_id: this zone's ID
        # owner_id: the player who owns this zone (-1 otherwise)
        # pods_p0: player 0's PODs on this zone
        # pods_p1: player 1's PODs on this zone
        # pods_p2: player 2's PODs on this zone (always 0 for a two player game)
        # pods_p3: player 3's PODs on this zone (always 0 for a two or three player game)
        z_id, owner_id, pods_p0, pods_p1, pods_p2, pods_p3 = [int(j) for j in input().split()]
        war_map[z_id] = {'owner': owner_id, 'pods': [pods_p0, pods_p1, pods_p2, pods_p3]}

    # Write an action using print
    # To debug: print >> sys.stderr, "Debug messages..."

    # first line for movement commands,
    for cell in war_map:
        if war_map[cell]['pods'][my_id] > 0:
            move += best_move(cell)
    if move == "":
        print("WAIT")
    else:
        print(move)

    buy = best_buy(platinum)

    #    second line for POD purchase (see the protocol in the statement for details)
    if buy == "":
        print("WAIT")
    else:
        print(buy)
    turn += 1