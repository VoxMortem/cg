import copy
import sys
import math


def print_perso(*args):
    chat = ""
    print(*args, chat)


def dist(x1, y1, x2, y2):
    return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)


def dist_obj(object1, object2):
    return dist(object1.x, object1.y, object2.x, object2.y)


class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Team(object):
    def __init__(self, id, health, mana, heroes):
        self.id = id
        self.health = health
        self.mana = mana
        self.heroes = heroes

    def __repr__(self):
        return 'Team {}: Health {}, Mana: {}'.format(self.id, self.health, self.mana)

    def debug(self):
        print('{}'.format(self), file=sys.stderr)

    def strat(self, monsters):
        if we_are_up:
            starting_pos = [(2500, 5500), (5500, 2500)]
        else:
            starting_pos = [(17630 - 2500, 9000 - 5500), (17630 - 5500, 9000 - 2500)]
        for i in range(len(heroes)):
            if i == 0:
                heroes[i].role = "ATTACK"
            else:
                heroes[i].role = i
                heroes[i].vx = starting_pos[i % 2][0]
                heroes[i].vy = starting_pos[i % 2][1]

    def act(self, monsters, evil, attack_mode):
        target = None
        new_attack_mode = attack_mode
        for hero in self.heroes:
            self.mana, target, tmp_attack_mode = hero.act(self.mana, monsters, team, evil, target, attack_mode)
            if tmp_attack_mode is not None:
                new_attack_mode = tmp_attack_mode
        return new_attack_mode

    def priority(self, monsters):
        return sorted(monsters, key=lambda x: x.base_dist)


class Monster(object):
    def __init__(self, id, x, y, shield_life, is_controlled, health, vx, vy, near_base, threat_for):
        self.id = id
        self.x = x
        self.y = y
        self.shield_life = shield_life
        self.is_controlled = is_controlled
        self.health = health
        self.vx = vx
        self.vy = vy
        self.near_base = near_base
        self.threat_for = threat_for
        self.priority = self._compute_priority()
        self.base_dist = dist(base_x, base_y, self.x, self.y)
        self.seen = True

    def _compute_priority(self):
        return (self.near_base == 1) * (self.threat_for == 1) * 1000 + (self.threat_for == 1) * 100

    def __repr__(self):
        return 'Monster {}: Position: {} {}, Shield: {}, IsControlled: {}, Health: {}, Direction: {} {}, NearBase: {}, ThreatFor: {}, Danger: {}'.format(
            self.id, self.x, self.y, self.shield_life, self.is_controlled, self.health, self.vx, self.vy,
            self.near_base, self.threat_for, self.get_danger())

    def debug(self):
        print('{}'.format(self), file=sys.stderr)

    def control(self, x, y):
        print_perso("SPELL", "CONTROL", self.id, x, y, "Overmind")

    def get_danger(self):
        if self.threat_for == 2:
            turn_to_base = -1 * (((dist(self.x, self.y, enemy_base_pos[0], enemy_base_pos[1]) - 299) // 400) + 1)
        elif self.threat_for == 1:
            turn_to_base = ((dist(self.x, self.y, base_x, base_y) - 299) // 400) + 1
        else:
            return 0
        return self.health / (2 * turn_to_base)

    def get_dangerous(self):
        if self.threat_for == 2:
            turn_to_base = -1 * (((dist(self.x, self.y, enemy_base_pos[0], enemy_base_pos[1]) - 299) // 400) + 1)
        elif self.threat_for == 1:
            turn_to_base = ((dist(self.x, self.y, base_x, base_y) - 299) // 400) + 1
        else:
            return 0
        return (self.health + self.shield_life) / (2 * turn_to_base)

    def next_turn(self):
        self.x += vx
        self.y += vy
        self.shield_life -= 1
        self.health -= 2
        if not (0 <= self.x <= 17630 and 0 <= self.y <= 9000):
            self.health = 0
        elif dist_obj(self, Point(0, 0)):
            vector = Point(-self.x * 400 / dist_obj(self, Point(0, 0)), -self.y * 400 / dist_obj(self, Point(0, 0)))
            self.vx = vector.x
            self.vy = vector.y
        elif dist_obj(self, Point(17630, 9000)):
            vector = Point((17630 - self.x) * 400 / dist_obj(self, Point(17630, 9000)),
                           (9000 - self.y) * 400 / dist_obj(self, Point(17630, 9000)))
            self.vx = vector.x
            self.vy = vector.y
        self.seen = False


class Hero(object):
    def __init__(self, id, x, y, vx, vy, shield_life, near_base, threat_for):
        self.id = id
        self.x = x
        self.y = y
        self.vx = vx
        self.vy = vy
        self.shield_life = shield_life
        self.near_base = near_base
        self.threat_for = threat_for
        self.role = None

    def __repr__(self):
        return 'Hero {}: Position: {} {},Direction: {} {}, NearBase: {}, ThreatFor: {}'.format(self.id, self.x, self.y,
                                                                                               self.vx, self.vy,
                                                                                               self.near_base,
                                                                                               self.threat_for)

    def debug(self):
        print('{}'.format(self), file=sys.stderr)

    def act(self, mana, monster, team, evil, target, attack_mode):
        if self.role == "ATTACK":
            return self.attack(mana, monster, evil, attack_mode)
        else:
            return self.defend(mana, monster, team, evil, target)

    def defend(self, mana, monsters, team, evil, target):
        opponents = sorted(evil.heroes, key=lambda x: dist_obj(x, our_base))
        ennemy_is_dangerous = False
        if opponents and dist_obj(opponents[0], our_base) < 6000 and evil.mana >= 10:
            ennemy_is_dangerous = True
        aim = team.priority(monsters)
        if (self.role % 2 != 0):
            if enemy_base_pos[0] != 0:
                aim = list(filter(lambda m: m.x < 9000 and m.y < 5000, aim))
            else:
                aim = list(filter(lambda m: m.x > 17630 - 9000 and m.y > 9000 - 5000, aim))
        else:
            if enemy_base_pos[0] != 0:
                aim = list(filter(lambda m: m.x < 5000 and m.y < 9000, aim))
            else:
                aim = list(filter(lambda m: m.x > 17630 - 5000 and m.y > 9000 - 5000, aim))
        if target and target.get_dangerous() <= 1 and not ennemy_is_dangerous:
            print(target.get_dangerous(), file=sys.stderr)
            print("ignore", file=sys.stderr)
            aim = list(filter(lambda x: x.id != target.id, aim))
        if len(aim) == 0:
            self.patrol(self.vx, self.vy, 400)
        else:
            should_control = aim[0].health > 15 and dist_obj(aim[0], self) < 2200 and dist_obj(our_base,
                                                                                               aim[0]) > 5500 and aim[
                                 0].get_dangerous() > 0
            should_push = ((dist_obj(aim[0], our_base) < 2800 and mana > 100) or dist_obj(aim[0], our_base) < 1000 or
                           aim[0].get_dangerous() >= 2) and dist_obj(aim[0], self) < 1280
            should_push_opponent = ennemy_is_dangerous and dist_obj(opponents[0], self) < 1280
            if should_control and aim[0].shield_life == 0 and mana > 10 and self.role % 2 != 0:
                aim[0].control(enemy_base_pos[0], enemy_base_pos[1])
                mana -= 10
            elif should_push_opponent and opponents[0].shield_life == 0 and mana > 10:
                print_perso("SPELL", "WIND", enemy_base_pos[0], enemy_base_pos[1], "Degage")
                mana -= 10
            elif should_push and aim[0].shield_life == 0 and mana > 10 and self.role % 2 == 0:
                print_perso("SPELL", "WIND", enemy_base_pos[0], enemy_base_pos[1], "Git Push")
                mana -= 10
            else:
                #print("MOVE", aim[0].x, aim[0].y)
                points = self.get_efficient_points(aim[0], aim, 800, 20, self)
                if points[0][1]>1 and False:
                    print_perso("MOVE", points[0][0].x, points[0][0].y, "Run fast Forest")
                else:
                    print_perso("MOVE", aim[0].x, aim[0].y, "Run Forest")
        return mana, aim[0] if aim else None, None

    def attack(self, mana, monsters, evil, attack_mode):
        new_attack_mode = attack_mode
        if attack_mode or turn >= 100 or mana > 150:
            new_attack_mode = True
            if dist_obj(self, enemy_base_point) > 9000:
                print("attacker patroling", file=sys.stderr)
                self.patrol(attack_point[0], attack_point[1], 1000)
            else:
                windable_enemy_heroes = [enemy for enemy in evil.heroes if
                                         dist_obj(self, enemy) <= 1280 and enemy.shield_life == 0]
                monsters_for_wind = [monster for monster in monsters if
                                     dist_obj(monster, self) <= 1280 and monster.shield_life == 0]
                goalable_monsters = [monster for monster in monsters_for_wind if dist_obj(
                                         enemy_base_point, monster) < 2500 and monster.health > 2 * len(
                                         windable_enemy_heroes)]
                if goalable_monsters and mana >= 10:
                    mana -= 10
                    print_perso("SPELL", "WIND", enemy_base_pos[0], enemy_base_pos[1], "BIM!")
                else:
                    close_monsters = sorted([monster for monster in monsters if dist_obj(monster, self) <= 2200],
                                            key=lambda x: dist_obj(self, x))
                    if close_monsters:
                        big_danger_monsters = sorted([monster for monster in monsters if dist_obj(monster,
                                                                                                  self) <= 2200 and monster.shield_life <= 1 and monster.threat_for == 2],
                                                     key=lambda x: x.get_danger())
                        shielded_monsters = [monster for monster in monsters if
                                             monster.shield_life > 0 and dist_obj(monster, self) < 3000]
                        farmable_monsters = [monster for monster in monsters if
                                             dist_obj(monster, self) <= 2200 and monster.threat_for != 2]

                        close_enemy_heroes = [enemy for enemy in evil.heroes if dist_obj(self, enemy) <= 2200]
                        monsters_for_wind = sorted(monsters_for_wind, key=lambda x: x.get_danger())
                        biggest_windable_monster = None
                        if monsters_for_wind:
                            biggest_windable_monster = copy.deepcopy(monsters_for_wind[0])
                            vector_x = (enemy_base_point.x-biggest_windable_monster.x)*2200/dist_obj(enemy_base_point,biggest_windable_monster)
                            vector_y = (enemy_base_point.y - biggest_windable_monster.y) * 2200 / dist_obj(enemy_base_point, biggest_windable_monster)
                            biggest_windable_monster.x += vector_x
                            biggest_windable_monster.y += vector_y
                        print("attacker:", len(big_danger_monsters), len(shielded_monsters), len(windable_enemy_heroes),
                              len(close_enemy_heroes), biggest_windable_monster, file=sys.stderr)
                        if big_danger_monsters and big_danger_monsters[0].get_danger() < min(-2, -len(close_enemy_heroes))/2 and mana >= 50:
                            mana -= 10
                            print_perso("SPELL SHIELD", big_danger_monsters[0].id, "SURPRISE MOFOS")
                        elif biggest_windable_monster and biggest_windable_monster.get_danger() < min(-2, -len(close_enemy_heroes))/2 and mana >= 10:
                            mana -= 10
                            print_perso("SPELL", "WIND", enemy_base_pos[0], enemy_base_pos[1])
                        elif len(shielded_monsters) > 0 and len(windable_enemy_heroes) > 0 and mana >= 50:
                            mana -= 10
                            print_perso("SPELL WIND", our_base.x, our_base.y, "GET REKT")
                        elif len(shielded_monsters) > 0:
                            next_enemy_point1 = Point(shielded_monsters[0].x - 2.1 * shielded_monsters[0].vx,
                                                      shielded_monsters[0].y - 2.1 * shielded_monsters[0].vy)
                            # next_enemy_point2 = Point(shielded_monsters[0].x + 3.1 * shielded_monsters[0].vx,
                            #                          shielded_monsters[0].y + 3.1 * shielded_monsters[0].vy)
                            # if dist_obj(self, next_enemy_point1)> dist_obj(self, next_enemy_point2):
                            #    print("MOVE", int(next_enemy_point2.x), int(next_enemy_point2.y), "PROTECT!")
                            # else:
                            print_perso("MOVE", int(next_enemy_point1.x), int(next_enemy_point1.y), "PROTECT!")
                        elif farmable_monsters:
                            big_life_monsters = sorted(farmable_monsters, key=lambda x: x.health, reverse=True)
                            if mana>200 and farmable_monsters[0].health>10:
                                print_perso("SPELL","CONTROL",big_life_monsters[0].id, enemy_base_point.x, enemy_base_point.y)
                            else:
                                center = self
                                granularity = 20
                                distance = 800
                                middle_point = enemy_base_point
                                points = self.get_efficient_points(center, farmable_monsters, distance, granularity,
                                                                   middle_point)
                                if points[0][1] > 0:
                                    print_perso("MOVE", points[0][0].x, points[0][0].y, "efficient farm")
                                else:
                                    print_perso("MOVE", farmable_monsters[0].x, farmable_monsters[0].y, "free farm!")
                                #print_perso("MOVE", farmable_monsters[0].x, farmable_monsters[0].y, "Free farm!")
                        else:
                            self.patrol(enemy_base_point.x, enemy_base_point.y, 3000)
                    else:
                        self.patrol(enemy_base_point.x, enemy_base_point.y, 3000)
        else:
            close_monsters = sorted([m for m in monsters if dist_obj(self, m) < 2200], key=lambda x: x.health)
            if close_monsters:
                center = self
                granularity = 20
                distance = 800
                middle_point = Point(17630 / 2, 9000 / 2)
                points = self.get_efficient_points(center, close_monsters, distance, granularity, middle_point)
                if points[0][1] > 0:
                    print_perso("MOVE", points[0][0].x, points[0][0].y, "efficient")
                else:
                    print_perso("MOVE", close_monsters[0].x, close_monsters[0].y, "welcome to the jungle")
            else:
                self.patrol(17630 // 2, 4500, 3000)
        return mana, None, new_attack_mode

    def get_efficient_points(self, center, close_monsters, distance, granularity, secondary_discriminant_point):
        points = []
        for i in range(-granularity, granularity):
            for j in range(-granularity, granularity):
                point = Point(int(center.x + (i * distance / granularity)),
                              int(center.y + (j * distance / granularity)))
                if dist_obj(center, point) <= distance:
                    monsters_nb = len(
                        [m for m in close_monsters if dist_obj(m, point) <= distance*0.95])
                    center_dist = dist_obj(point, secondary_discriminant_point)
                    points.append((point, monsters_nb, center_dist))
        points = sorted(points, key=lambda x: (-x[1], x[2]))
        return points

    def patrol(self, x, y, patrol_range):
        x = min(max(x, patrol_range), 17630 - patrol_range)
        y = min(max(y, patrol_range), 9000 - patrol_range)
        patrol_checkpoints = [[x - patrol_range, y - patrol_range], [x + patrol_range, y - patrol_range],
                              [x + patrol_range, y + patrol_range], [x - patrol_range, y + patrol_range]]
        closest = 0
        for i in range(len(patrol_checkpoints)):
            ckpt = patrol_checkpoints[i]
            if dist(self.x, self.y, ckpt[0], ckpt[1]) <= dist(self.x, self.y, patrol_checkpoints[closest][0],
                                                              patrol_checkpoints[closest][1]):
                closest = i
        print_perso("MOVE", patrol_checkpoints[(closest + 1) % len(patrol_checkpoints)][0],
                    patrol_checkpoints[(closest + 1) % len(patrol_checkpoints)][1])


if __name__ == '__main__':

    base_x, base_y = [int(i) for i in input().split()]
    our_base = Point(base_x, base_y)
    heroes_per_player = int(input())
    attack_mode = False

    if base_x == 0:
        we_are_up = True
        enemy_base = [[12630, 17630], [4000, 9000]]
        enemy_base_pos = [17630, 9000]
        enemy_base_point = Point(17630, 9000)
        attack_point = [12630, 4000]
    else:
        we_are_up = False
        enemy_base = [[0, 5000], [0, 5000]]
        enemy_base_pos = [0, 0]
        enemy_base_point = Point(0, 0)
        attack_point = [5000, 5000]

    # game loop
    turn = 0
    monsters = []
    while True:
        print(we_are_up, enemy_base, enemy_base_pos, attack_point, file=sys.stderr)
        turn += 1
        healths = [0, 0]
        manas = [0, 0]
        for i in range(2):
            healths[i], manas[i] = [int(j) for j in input().split()]
        print(healths, file=sys.stderr)
        print(manas, file=sys.stderr)
        entity_count = int(input())
        heroes = list()
        new_monsters = []
        new_monsters_ids = []
        enemy_heroes = []
        for i in range(entity_count):
            id, type, x, y, shield_life, is_controlled, health, vx, vy, near_base, threat_for = [int(j) for j in
                                                                                                 input().split()]
            if type == 1:
                hero = Hero(id, x, y, vx, vy, shield_life, near_base, threat_for)
                heroes.append(hero)
            elif type == 2:
                hero = Hero(id, x, y, vx, vy, shield_life, near_base, threat_for)
                enemy_heroes.append(hero)
            else:
                monster = Monster(id, x, y, shield_life, is_controlled, health, vx, vy, near_base, threat_for)
                monster.debug()
                new_monsters.append(monster)
                new_monsters_ids.append(id)
        for monster in monsters:
            break
            if monster.id not in new_monsters_ids:
                monster.next_turn()
                distances = [dist_obj(monster, hero) for hero in heroes]
                if monster.health > 0 and min(distances) > 2200 and dist_obj(monster, our_base):
                    new_monsters.append(monster)
        monsters = new_monsters
        team = Team('Apes', healths[0], manas[0], heroes)
        team.debug()
        evil = Team('Bouh', healths[1], manas[1], enemy_heroes)

        team.strat(monsters)
        attack_mode = team.act(monsters, evil, attack_mode)
