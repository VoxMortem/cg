import sys
import math

right=None
thrust = (7 * 100 + 500) / 8

def distance(pointA, pointB):
    x = pointB[0]-pointA[0]
    y = pointB[1]-pointA[1]
    return math.sqrt(pow(x,2)+pow(y,2))

def equi_dist(iPoint1, iPoint2):
    return int((iPoint1[0]+iPoint2[0])/2), int((iPoint1[1]+iPoint2[1])/2)

def find_defense_position(iOurFlag, iPod, iEnnemies):
    if distance(iOurFlag, iEnnemies[0]) < distance(iOurFlag, iEnnemies[1]):
        # Ennemi #0 is the closest
        return equi_dist(iOurFlag, iEnnemies[0])
    else:
        # Ennemi #1 is the closest
        return equi_dist(iOurFlag, iEnnemies[1])

def check_collision(iFlagged, iOther):
    if math.fabs(iFlagged[1] - iOther[1]) <= 1500:
        # The two pods are too close on the Y axis, evasion action must be taken

        # remark: take speed into account ?
        if iFlagged[1] >= iOther[1]:
            # The pod with the flag is lower than the other, go a little further down -> 1
            return True, 1
        else:
            # The pod with the flag is higher than the other, go a little further up -> -1
            return True, -1
    return False, 0

def computeTurns(dist, speed, turn = 0):
    new_speed = speed * 0.9 + thrust
    turns_left = math.floor(dist / new_speed) + 1
    if turns_left != 1:
        turn += 1
        return computeTurns(dist = dist - new_speed, speed = new_speed, turn = turn)
    else:
        return turn

def speed(vx,vy):
    return math.sqrt(vx**2+vy**2)

def compute_turns_area(pod, targetx, turn = 0):
    if pod[0] == targetx:
        return turn
    turn += 1
    thrust = 100 if pod[0]<targetx else -100
    newvx = pod[2]*0.9 + thrust
    newvy = pod[3]
    newx = pod[0] + newvx
    newy = pod[1] + newvy
    new_pod = [newx,newy,newvx,newvy,pod[4]]
    if (pod[0]<=targetx<=newx) or (pod[0]>=targetx>=newx):
        return turn
    else:
        return compute_turns_area(new_pod,targetx,turn)

def optimize_trajectory(pod,target):
    destx = target[0]-pod[2]
    desty = target[1]-pod[3]
    return destx,desty


def compute_range(turns):
    result = {}
    result[0] = [0, 0]
    range = 0
    turn = 1
    speed = 0
    result[turn] = [speed, range]
    for i in xrange(turns):
        turn += 1
        speed = round(0.9 * speed) + thrust
        range += speed
        result[turn]= [speed, range]
    return result

def compute_turn(pod,target):
    i=0
    new_pod = pod[:]
    targetx = 1000 if right else 9000
    turns_left = compute_turns_area(target, targetx, turn = 0)
    turns_left = 20
    ranges = compute_range(turns_left)
    while(distance(new_pod,target)>ranges[i][1]):
        i += 1
        newvx, newvy, newx, newy = rebound(new_pod)
        new_pod = [newx,newy,newvx,newvy,new_pod[4]]
        if i>turns_left:
            return 99
    return i


def rebound(new_pod):
    if new_pod[0] + new_pod[2] < 400:
        newx = 800 - (new_pod[0] + new_pod[2])
        newvx = -new_pod[2]
    elif new_pod[0] + new_pod[2] > 9600:
        newx = 19200 - (new_pod[0] + new_pod[2])
        newvx = -new_pod[2]
    else:
        newx = new_pod[0] + new_pod[2]
        newvx = new_pod[2]
    if new_pod[1] + new_pod[3] < 400:
        newy = 800 - (new_pod[1] + new_pod[3])
        newvy = -new_pod[3]
    elif new_pod[1] + new_pod[3] > 7600:
        newy = 15200 - (new_pod[1] + new_pod[3])
        newvy = -new_pod[3]
    else:
        newy = new_pod[1] + new_pod[3]
        newvy = new_pod[3]
    return newvx, newvy, newx, newy


def intercept(pod,enemy_pod,enemy_flag_x,enemy_flag_y):
    i=0
    thrust = 100
    if distance(pod,enemy_pod)<1500:
        destx = enemy_pod[0] + enemy_pod[2] - pod[2]
        desty = enemy_pod[1] + enemy_pod[3] - pod[3]
        return [destx, desty, "BOOST"]
        return optimize_trajectory(pod,)
    new_enemy_pod = enemy_pod[:]
    targetx = 1000 if right else 9000
    turns_left = compute_turns_area(new_enemy_pod, targetx)
    while(i<compute_turn(pod,new_enemy_pod)):
        i += 1
        newvx, newvy, newx, newy = rebound(new_enemy_pod)
        new_enemy_pod = [newx,newy,newvx,newvy, new_enemy_pod[4]]
        #if i>turns_left:
        #    return optimal_attack_move(pod, enemy_flag_x, enemy_flag_y, 0, None)
    if i>=8:
        thrust = "BOOST"
    if i<2:
        if speed(pod[2], pod[3])*0.9+500 > speed(enemy_pod[2], enemy_pod[3]):
            return enemy_pod[0], enemy_pod[1], "BOOST"
        return enemy_pod[0], enemy_pod[1], thrust
    aimx = new_enemy_pod[0]
    aimy = new_enemy_pod[1]
    return aimx,aimy,thrust

def compute_turns_dir(pod, target):
    range = 100
    turn = 1
    speed = 0
    while distance(pod, target) > range :
        turn += 1
        speed = 0.9*speed + 100
        range += speed
    return turn

def distance_point_droite(pod,target):
    # ux est targetx - podx
    # uy est targety - pody
    # vx est vx
    # vy est vy
    cosx = ((target[0]-pod[0])*pod[2] + (target[1]-pod[1])*pod[3])/( distance(pod,target) * math.sqrt(pod[2]**2+pod[3]**2))
    result = distance(pod,target) * cosx
    return result

def defense_behaviour(iPod,iFlag):
    aEnnemi1 = iPod[0]
    aEnnemi2 = iPod[1]

    aDistanceFlag1 = distance(iFlag, aEnnemi1)
    aDistanceFlag2 = distance(iFlag, aEnnemi2)

    # status is the behavior of the defense pod
    aStatus = "StandBy"

    # aThreat is Ennemi#1, Ennemi#2 or both
    aThreat = []

    # string consts
    aEnnemy1Str = "Ennemi1"
    aEnnemy2Str = "Ennemi2"

    if (aEnnemi1[4]):
        # Ennemi 1 has the flag
        print >> sys.stderr, "Ennemi 1 Intercept"
        aStatus = "Intercept"
        aThreat = [0]

    elif (aEnnemi2[4]):
        # Ennemi 1 has the flag
        print >> sys.stderr, "Ennemi 2 Intercept"
        aStatus = "Intercept"
        aThreat = [1]

    else:
        if (aDistanceFlag1 < 4000):
            print >> sys.stderr, "Ennemi 1 in dangerous"
            aStatus = "BodyBlock"
            if 0 not in aThreat:
                aThreat.append(0)

        if (aDistanceFlag2 < 4000):
            print >> sys.stderr, "Ennemi 2 in dangerous"
            aStatus = "BodyBlock"
            if 1 not in aThreat:
                aThreat.append(1)

        if len(aThreat) > 1:
            print >> sys.stderr, "Both are dangerous"

    return aStatus, aThreat

def optimal_defense_move(enemy_pods,flagx,flagy,pod,enemy_flag_x,enemy_flag_y):
    flag = (flagx,flagy)
    status, enemyids = defense_behaviour(enemy_pods, flag)
    print >> sys.stderr,status, enemyids
    if status == "Intercept":
        enemyid = enemyids[0]
        destx,desty,thrust = intercept(pod,enemy_pods[enemyid],enemy_flag_x,enemy_flag_y)
        return [destx,desty,"BOOST"]
    else:
        aim = optimize_trajectory(pod,find_defense_position(flag,pod,enemy_pods))
        return [aim[0],aim[1],100]


def check_collision(iFlagged, iOther):
    if math.fabs(iFlagged[1] - iOther[1]) <= 800:
        # The two pods are too close on the Y axis, evasion action must be taken

        # remark: take speed into account ?
        if iFlagged[1] >= iOther[1]:
            # The pod with the flag is lower than the other, go a little further down -> 1
            return True, 1
        else:
            # The pod with the flag is higher than the other, go a little further up -> -1
            return True, -1
    return False, 0


def optimal_attack_move(pod, enemy_flag_x, enemy_flag_y, turnboost, other_pod,enemy_pods):
    thrust = 100
    if enemy_flag_x==1000 and pod[0]>enemy_flag_x and pod[2]>-100:
        enemy_flag_x = -200
    elif enemy_flag_x == 9000 and pod[0]<enemy_flag_x and pod[2]<100:
        enemy_flag_x = 10200
    if pod[4]:
        if right:
            destx = 9000
            if compute_turns_area(pod,9000)>compute_turns_area(pod,-8600):
                destx = -8600
        else:
            destx = 1000
            if compute_turns_area(pod,1000)>compute_turns_area(pod,18200):
                destx = 18200
        desty = pod[1] + int(math.fabs(pod[0] - destx) * check_collision(pod, other_pod)[1])
        if right:
            half = [pod[0],10000]
        else:
            half = [0,pod[0]]
        print >> sys.stderr, half
        top_entites = 0
        bot_entities = 0
        bot_friend = False
        if (half[0]<other_pod[0]<half[1]):
            if other_pod[1]>4000:
                bot_entities += 1
                bot_friend = True
            else:
                top_entites += 1
        for en in enemy_pods:
            print >> sys.stderr, en
            if (half[0] < en[0] < half[1]):
                if en[1] > 4000:
                    bot_entities += 1
                else:
                    top_entites += 1
        if bot_entities == 0 and top_entites == 0:
            desty = pod[1] + int(math.fabs(pod[0]-destx)*check_collision(pod, other_pod)[1])
        elif bot_entities == 0:
            desty = 7000
        elif top_entites == 0:
            desty = 1000
        else:
            desty = pod[1] + int(math.fabs(pod[0] - destx) * check_collision(pod, other_pod)[1])
    else:
        destx, desty = optimize_trajectory(pod,(enemy_flag_x,enemy_flag_y))
        if computeTurns(distance(pod, (enemy_flag_x,enemy_flag_y)), speed(pod[2],pod[3]))<=1:
            thrust = "BOOST"
    return [destx,desty,thrust]

# game loop
while True:
    inp = raw_input()
    print >> sys.stderr, inp
    flag_x, flag_y = [int(i) for i in inp.split()]
    if right is None:
        if flag_x == 1000:
            right = False
        else:
            right = True
    inp = raw_input()
    print >> sys.stderr, inp
    enemy_flag_x, enemy_flag_y = [int(i) for i in inp.split()]
    own_pods = []
    for i in xrange(2):
        inp = raw_input()
        print >> sys.stderr, inp
        x, y, vx, vy, flag = [int(j) for j in inp.split()]
        own_pods.append([x, y, vx, vy, flag])
    enemy_pods = []
    for i in xrange(2):
        inp = raw_input()
        print >> sys.stderr, inp
        x, y, vx, vy, flag = [int(j) for j in inp.split()]
        enemy_pods.append([x, y, vx, vy, flag])
        if flag:
            flag_x=x
            flag_y=y
    destinations = []
    if enemy_flag_x == -1:
        if own_pods[0][4]:
            destinations.append(optimal_attack_move(own_pods[0],enemy_flag_x,enemy_flag_y,0, own_pods[1],enemy_pods))
            destinations.append(optimal_defense_move(enemy_pods,flag_x,flag_y,own_pods[1],enemy_flag_x,enemy_flag_y))
        else:
            destinations.append(optimal_defense_move(enemy_pods,flag_x,flag_y,own_pods[0],enemy_flag_x,enemy_flag_y))
            destinations.append(optimal_attack_move(own_pods[1], enemy_flag_x, enemy_flag_y, 0, own_pods[0],enemy_pods))
    else:
        if distance(own_pods[0],(flag_x,flag_y))>distance(own_pods[1],(flag_x,flag_y)):
            destinations.append(optimal_attack_move(own_pods[0], enemy_flag_x, enemy_flag_y, 0, own_pods[1],enemy_pods))
            destinations.append(optimal_defense_move(enemy_pods,flag_x,flag_y,own_pods[1],enemy_flag_x,enemy_flag_y))
        else:
            destinations.append(optimal_defense_move(enemy_pods,flag_x,flag_y,own_pods[0],enemy_flag_x,enemy_flag_y))
            destinations.append(optimal_attack_move(own_pods[1], enemy_flag_x, enemy_flag_y, 0, own_pods[0],enemy_pods))

    for i in destinations:
        print i[0],i[1],i[2]
