import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.
right=None

thrust = (7 * 100 + 500) / 8

def distance(pointA, pointB):
    x = pointB[0]-pointA[0]
    y = pointB[1]-pointA[1]
    return math.sqrt(pow(x,2)+pow(y,2))

def getGoal(ibAllied):
    if (right and not ibAllied) or (not right and ibAllied):
        return 1000
    else:
        return 9000

def equi_dist(iPoint1, iPoint2):
    return int((iPoint1[0]+iPoint2[0])/2), int((iPoint1[1]+iPoint2[1])/2)

def find_defense_position(iOurFlag, iPod, iEnnemies):
    if distance(iOurFlag, iEnnemies[0]) < distance(iOurFlag, iEnnemies[1]):
        # Ennemi #0 is the closest
        return equi_dist(iOurFlag, iEnnemies[0])
    else:
        # Ennemi #1 is the closest
        return equi_dist(iOurFlag, iEnnemies[1])

def check_collision(iFlagged, iOther):
    if math.fabs(iFlagged[1] - iOther[1]) <= 800:
        # The two pods are too close on the Y axis, evasion action must be taken

        # remark: take speed into account ?
        if iFlagged[1] >= iOther[1]:
            # The pod with the flag is lower than the other, go a little further down -> 1
            return True, 1
        else:
            # The pod with the flag is higher than the other, go a little further up -> -1
            return True, -1
    return False, 0

def computeTurns(dist, speed, turn = 0):
    new_speed = speed * 0.9 + thrust
    turns_left = math.floor(dist / new_speed) + 1
    if turns_left != 1:
        turn += 1
        return computeTurns(dist = dist - new_speed, speed = new_speed, turn = turn)
    else:
        return turn

def speed(vx,vy):
    return math.sqrt(vx**2+vy**2)

def compute_turns_area(pod, targetx, turn = 0):
    if pod[0] == targetx:
        return turn
    turn += 1
    thrust = 100 if pod[0]<targetx else -100
    newvx = pod[2]*0.9 + thrust
    newvy = pod[3]
    newx = pod[0] + newvx
    newy = pod[1] + newvy
    new_pod = [newx,newy,newvx,newvy,pod[4]]
    if (pod[0]<=targetx<=newx) or (pod[0]>=targetx>=newx):
        return turn
    else:
        return compute_turns_area(new_pod,targetx,turn)

def optimize_trajectory(pod,target):
    destx = target[0]-pod[2]
    desty = target[1]-pod[3]
    return destx,desty


def compute_range(turns):
    result = {}
    result[0] = [0, 0]
    range = 0
    turn = 1
    speed = 0
    result[turn] = [speed, range]
    for i in xrange(turns):
        turn += 1
        speed = round(0.9 * speed) + thrust
        range += speed
        result[turn]= [speed, range]
    return result

def compute_turn(pod,target):
    i=0
    new_pod = pod[:]
    targetx = 1000 if right else 9000
    turns_left = compute_turns_area(target, targetx, turn = 0)
    turns_left = 20
    ranges = compute_range(turns_left)
    while(distance(new_pod,target)>ranges[i][1]):
        i += 1
        newvx, newvy, newx, newy = rebound(new_pod)
        new_pod = [newx,newy,newvx,newvy,new_pod[4]]
        if i>turns_left:
            return 99
    return i


def rebound(new_pod):
    if new_pod[0] + new_pod[2] < 400:
        newx = 800 - (new_pod[0] + new_pod[2])
        newvx = -new_pod[2]
    elif new_pod[0] + new_pod[2] > 9600:
        newx = 19200 - (new_pod[0] + new_pod[2])
        newvx = -new_pod[2]
    else:
        newx = new_pod[0] + new_pod[2]
        newvx = new_pod[2]
    if new_pod[1] + new_pod[3] < 400:
        newy = 800 - (new_pod[1] + new_pod[3])
        newvy = -new_pod[3]
    elif new_pod[1] + new_pod[3] > 7600:
        newy = 15200 - (new_pod[1] + new_pod[3])
        newvy = -new_pod[3]
    else:
        newy = new_pod[1] + new_pod[3]
        newvy = new_pod[3]
    return newvx, newvy, newx, newy


def intercept(pod,enemy_pod,enemy_flag_x,enemy_flag_y):
    i=0
    thrust = 100
    new_enemy_pod = enemy_pod[:]
    targetx = 1000 if right else 9000
    turns_left = compute_turns_area(new_enemy_pod, targetx)
    while(i<compute_turn(pod,new_enemy_pod)):
        i += 1
        newvx, newvy, newx, newy = rebound(new_enemy_pod)
        new_enemy_pod = [newx,newy,newvx,newvy, new_enemy_pod[4]]
        #if i>turns_left:
        #    return optimal_attack_move(pod, enemy_flag_x, enemy_flag_y, 0, None)
    if i>=8:
        thrust = "BOOST"
    if i<2:
        if speed(pod[2], pod[3])*0.9+500 > speed(enemy_pod[2], enemy_pod[3]):
            return enemy_pod[0], enemy_pod[1], "BOOST"
        return enemy_pod[0], enemy_pod[1], thrust
    aimx = new_enemy_pod[0]
    aimy = new_enemy_pod[1]
    return aimx,aimy,thrust

def compute_turns_dir(pod, target):
    range = 100
    turn = 1
    speed = 0
    while distance(pod, target) > range :
        turn += 1
        speed = 0.9*speed + 100
        range += speed
    return turn

def distance_point_droite(pod,target):
    # ux est targetx - podx
    # uy est targety - pody
    # vx est vx
    # vy est vy
    cosx = ((target[0]-pod[0])*pod[2] + (target[1]-pod[1])*pod[3])/( distance(pod,target) * math.sqrt(pod[2]**2+pod[3]**2))
    result = distance(pod,target) * cosx
    return result

def defense_behaviour(iPod,iFlag):
    aEnnemi1 = iPod[0]
    aEnnemi2 = iPod[1]

    aDistanceFlag1 = distance(iFlag, aEnnemi1)
    aDistanceFlag2 = distance(iFlag, aEnnemi2)

    # status is the behavior of the defense pod
    aStatus = "StandBy"

    # aThreat is Ennemi#1, Ennemi#2 or both
    aThreat = []

    # string consts
    aEnnemy1Str = "Ennemi1"
    aEnnemy2Str = "Ennemi2"

    if (aEnnemi1[4]):
        # Ennemi 1 has the flag
        print >> sys.stderr, "Ennemi 1 Intercept"
        aStatus = "Intercept"
        aThreat = [0]

    elif (aEnnemi2[4]):
        # Ennemi 1 has the flag
        print >> sys.stderr, "Ennemi 2 Intercept"
        aStatus = "Intercept"
        aThreat = [1]

    else:
        if (aDistanceFlag1 < 4000):
            print >> sys.stderr, "Ennemi 1 in dangerous"
            aStatus = "BodyBlock"
            if 0 not in aThreat:
                aThreat.append(0)

        if (aDistanceFlag2 < 4000):
            print >> sys.stderr, "Ennemi 2 in dangerous"
            aStatus = "BodyBlock"
            if 1 not in aThreat:
                aThreat.append(1)

        if len(aThreat) > 1:
            print >> sys.stderr, "Both are dangerous"

    return aStatus, aThreat

def optimal_defense_move(enemy_pods,flagx,flagy,pod,enemy_flag_x,enemy_flag_y):
    flag = (flagx,flagy)
    status, enemyids = defense_behaviour(enemy_pods, flag)
    print >> sys.stderr,status, enemyids
    if status == "Intercept":
        enemyid = enemyids[0]
        destx,desty,thrust = intercept(pod,enemy_pods[enemyid],enemy_flag_x,enemy_flag_y)
        return [destx,desty,thrust]
    elif status == "StandBy":
        return [optimize_trajectory(pod,find_defense_position(flag,pod,enemy_pods)),100]
    else:
        return [flagx, flagy, 100]


def check_collision(iFlagged, iOther):
    if math.fabs(iFlagged[1] - iOther[1]) <= 800:
        # The two pods are too close on the Y axis, evasion action must be taken

        # remark: take speed into account ?
        if iFlagged[1] >= iOther[1]:
            # The pod with the flag is lower than the other, go a little further down -> 1
            return True, 1
        else:
            # The pod with the flag is higher than the other, go a little further up -> -1
            return True, -1
    return False, 0


def optimal_attack_move(pod, enemy_flag_x, enemy_flag_y, turnboost, other_pod):
    thrust = 100
    if enemy_flag_x==1000:
        enemy_flag_x = -200
    elif enemy_flag_x == 9000:
        enemy_flag_x = 10200
    print "flag_x:",enemy_flag_x
    if pod[4]:
        if right:
            destx = 9000
            if compute_turns_area(pod,9000)>compute_turns_area(pod,-8600):
                destx = -8600
        else:
            destx = 1000
            if compute_turns_area(pod,1000)>compute_turns_area(pod,18200):
                destx = 18200
        print "destx:", destx
        desty = pod[1] + int(math.fabs(pod[0]-destx)*check_collision(pod, other_pod)[1])
    else:
        destx, desty = optimize_trajectory(pod,(enemy_flag_x,enemy_flag_y))
        if computeTurns(distance(pod, (enemy_flag_x,enemy_flag_y)), speed(pod[2],pod[3]))<=1:
            thrust = "BOOST"
    return [destx,desty,thrust]
