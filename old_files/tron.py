import sys
import math

global_map = []
length = 30
height = 20

for i in range(length):
    global_map.append([])
    for j in range(height):
        global_map[i].append(-1)


def update_map(player, position, global_map):
    global_map[position[0]][position[1]] = player


def isPositionSafe(iPosX, iPosY):
    if iPosX >= 0 and iPosX < length:
        if iPosY >= 0 and iPosY < height:
            if global_map[iPosX][iPosY] == -1:
                return True
            else:
                pass
                #print >> sys.stderr, "Not safe : ", global_map[iPosX][iPosY]
    return False


def getCloser(iPlayerX, iPlayerY, iMechantX, iMechantY):
    # print >> sys.stderr, iPlayerX, iPlayerY, iMechantX, iMechantY
    aMedianX = abs(iMechantX + iPlayerX) / 2
    aMedianY = abs(iMechantY + iPlayerY) / 2
    #print >> sys.stderr, "Median", aMedianX, aMedianY

    if abs(aMedianX - iPlayerX) > abs(aMedianY - iPlayerY):
        if aMedianX - iPlayerX > 0:
            #print >> sys.stderr, "Droite"
            return [iPlayerX + 1, iPlayerY]
        else:
            #print >> sys.stderr, "gauche"
            return [iPlayerX - 1, iPlayerY]
    else:
        if aMedianY > iPlayerY:
            #print >> sys.stderr, "bas", iPlayerX, iPlayerY
            return [iPlayerX, iPlayerY + 1]
        else:
            #print >> sys.stderr, "up"
            return [iPlayerX, iPlayerY - 1]


def get_direction(previous_pos, next_pos):
    if previous_pos[0] == next_pos[0] and previous_pos[1] - 1 == next_pos[1]:
        return "UP"
    elif previous_pos[0] == next_pos[0] and previous_pos[1] == next_pos[1] - 1:
        return "DOWN"
    elif previous_pos[0] == next_pos[0] + 1 and previous_pos[1] == next_pos[1]:
        return "LEFT"
    elif previous_pos[0] == next_pos[0] - 1 and previous_pos[1] == next_pos[1]:
        return "RIGHT"
    else:
        return None


def getDistance(iPlayerPos, iOpPos):
    return math.sqrt(abs(iOpPos[0] - iPlayerPos[0]) ** 2 + abs(iOpPos[1] - iPlayerPos[1]) ** 2)


def findQuadrant(iPlayerPos, iOpPos):
    if iPlayerPos[0] == iOpPos[0]:
        if iPlayerPos[1] > iOpPos[1]:
            return ["DOWN", ""]
        else:
            return ["UP", ""]
    if iPlayerPos[1] == iOpPos[1]:
        if iPlayerPos[0] > iOpPos[0]:
            return ["LEFT", ""]
        else:
            return ["RIGHT", ""]

    if iPlayerPos[0] > iOpPos[0] and iPlayerPos[1] > iOpPos[1]:
        return ["UP", "LEFT"]
    elif iPlayerPos[0] < iOpPos[0] and iPlayerPos[1] > iOpPos[1]:
        return ["UP", "RIGHT"]
    elif iPlayerPos[0] > iOpPos[0] and iPlayerPos[1] < iOpPos[1]:
        return ["DOWN", "LEFT"]
    elif iPlayerPos[0] < iOpPos[0] and iPlayerPos[1] < iOpPos[1]:
        return ["DOWN", "RIGHT"]


aPosition = {}


def fill(iPosX, iPosY, direction, previous):
    #print >> sys.stderr, str(iPosX) + ', ' + str(iPosY)
    #print >> sys.stderr, global_map[iPosX][iPosY]
    incrX = 0
    if direction == 'LEFT':
        incrX -= 1
    elif direction == 'RIGHT':
        incrX += 1
    else:
        if previous == 'LEFT':
            previous = 'RIGHT'
            direction = 'RIGHT'
            incrX += 1
        elif previous == 'RIGHT':
            previous = 'LEFT'
            direction = 'LEFT'
            incrX -= 1
    incrY = 0
    if oord == 'DOWN':
        incrY += 1
    else:
        incrY -= 1
    #print >> sys.stderr, previous
    #print >> sys.stderr, direction
    if isPositionSafe(iPosX + incrX, iPosY):
        if isPositionSafe(iPosX + incrX, iPosY + incrY) or not isPositionSafe(iPosX, iPosY):
            return direction, previous
        else:
            return oord, previous
    else:
        if isPositionSafe(iPosX + incrX, iPosY + incrY):
            return oord, previous
        else:
            return oord, previous


direction = 'LEFT'
previous = 'LEFT'
aggressive = True
oord = 'DOWN'


def main():
    global aggressive, direction, previous
    if not aggressive or dist < 3:
        aggressive = False
        direction, previous = fill(aPlayerPos[0], aPlayerPos[1], direction, previous)
        print(direction)
    else:
        aNewPos = getCloser(aPlayerPos[0], aPlayerPos[1], aOpPos[0], aOpPos[1])
        if not isPositionSafe(aNewPos[0], aNewPos[1]):
            # print("NOT SAFE", aNewPos, file=sys.stderr)
            aggressive = False
            direction, previous = fill(aPlayerPos[0], aPlayerPos[1], direction, previous)
            print(direction)
        else:
            direction = get_direction(aPlayerPos, aNewPos)
            print(get_direction(aPlayerPos, aNewPos))

            # Write an action using print
            # To debug: print >> sys.stderr, "Debug messages..."

            # A single line with UP, DOWN, LEFT or RIGHT
            # print "LEFT"


# game loop
while 1:
    # n: total number of players (2 to 4).
    # p: your player number (0 to 3).
    inp=input()
    print(inp, file=sys.stderr)
    n, p = [int(i) for i in inp.split()]
    aPlayerPos = []
    aOpPos = []
    previous_pos = None

    for i in range(n):
        # x0: starting X coordinate of lightcycle (or -1)
        # y0: starting Y coordinate of lightcycle (or -1)
        # x1: starting X coordinate of lightcycle (can be the same as X0 if you play before this player)
        # y1: starting Y coordinate of lightcycle (can be the same as Y0 if you play before this player)
        inp = input()
        print(inp, file=sys.stderr)
        x0, y0, x1, y1 = [int(j) for j in inp.split()]
        update_map(i, (x0, y0), global_map)
        update_map(i, (x1, y1), global_map)
        if i == p:
            aPlayerPos = [x1, y1]
            if x0 == x1 and y0 == y1:
                if y0 > height / 2:
                    oord = 'UP'
                else:
                    oord = 'DOWN'
        else:
            aOpPos = [x1, y1]
            if previous_pos:
                adv_dir = get_direction(previous_pos, aOpPos)
            previous_pos = aOpPos

            # print >> sys.stderr, "InitialX", x0, "InitialY", y0, "CurrentX", x1, "CurrentY", y1

    dist = getDistance(aPlayerPos, aOpPos)
    #print >> sys.stderr, findQuadrant(aPlayerPos, aOpPos)
    #print >> sys.stderr, "is aggressive ", aggressive

    main()