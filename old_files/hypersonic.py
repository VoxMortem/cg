import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

def get_input():
    return input()

def expl(x,y,dmap,width,height):
    result = []
    for i in range(x+1,min(x+2,width)):
        if dmap[y][i]=="0":
            result.append((i,y))
            break
    for i in reversed(range(max(x-2,0),x-1)):
        if dmap[y][i]=="0":
            result.append((i,y))
            break
    for i in range(y+1,min(y+2,height)):
        if dmap[i][x]=="0":
            result.append((x,i))
            break
    for i in reversed(range(max(y-2,0),y-1)):
        if dmap[i][x]=="0":
            result.append((x,i))
            break
    return result


def number_expl(x,y,dmap,width,height):
    return len(expl(x,y,dmap,width,height))


def dist(cell1, cell2):
    return abs(cell1[0]-cell2[0])+abs(cell1[1]-cell2[0])


def best_move(dmap,width,height):
    cells = {}
    aim = None
    for i in range(width):
        for j in range(height):
            if dmap[j][i] == '.':
                numb = number_expl(i,j,dmap,width,height)
                print("cell ",i,j,numb, file=sys.stderr)
                if not numb in cells:
                    cells[numb]=[]
                cells[numb].append((i,j))
    print("max cells ",cells.keys(), file=sys.stderr)
    print("cells are",cells[max(cells.keys())], file=sys.stderr)
    for cell in cells[max(cells.keys())]:
        if not aim or dist(aim, (x,y))>dist(cell, (x,y)):
            aim = cell
    return aim[0],aim[1]


def main():
    width, height, my_id = [int(i) for i in get_input().split()]
    # game loop
    while True:
        print("hey sucker")
        dmap = []
        for i in range(height):
            dmap.append(list(get_input()))
        players = {}
        bombs = []
        objects = []
        entities_nb = int(get_input())
        for i in range(entities_nb):
            entity_type, owner, x, y, param_1, param_2 = [int(j) for j in get_input().split()]
            if entity_type == 0:
                player = {"x":x, "y":y, "bombs_left":param_1, "range":param_2}
                players[owner] = player
            elif entity_type == 1:
                bomb = {"owner":owner, "x":x, "y":y, "turn_left":param_1, "range":param_2}
                bombs.append(bomb)
            else:
                obj = {"owner":owner, "x":x, "y":y, "type":param_1, "param_2":param_2}
                objects.append(obj)

        # Write an action using print
        # To debug: print("Debug messages...", file=sys.stderr)
        print("2Dmap",dmap, file=sys.stderr)
        print("bombs",bombs, file=sys.stderr)
        print("players",players, file=sys.stderr)
        print("objects",objects, file=sys.stderr)
        order = "MOVE"
        if number_expl(players[my_id]["x"],players[my_id]["y"],dmap,width,height)>1:
            order = "BOMB"
        print("order is",order,"because",number_expl(players[my_id]["x"],players[my_id]["y"],dmap,width,height), file=sys.stderr)
        if len(objects)>0:
            aimx = objects[0]["x"]
            aimy = objects[0]["y"]
        else:
            aimx, aimy = best_move(dmap,width,height)
        print("move is",aimx, aimy, file=sys.stderr)
        print(order,aimx, aimy)

main()